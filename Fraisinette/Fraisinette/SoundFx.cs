﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

namespace Fraisinette
{
    public static class SoundFx
    {


        //public static enum SoundFxState { playing, stop };
        //public static SoundFxState soundFxState = new SoundFxState();
        
        static SoundEffect Jump;
        static SoundEffectInstance JumpInstance;
        static SoundEffect Straw;
        static SoundEffect Leaf2;
        static SoundEffect Birds;
        static SoundEffect EnterIn;
        static SoundEffect EnterOut;
        static SoundEffect LevelEnd4;
        static SoundEffectInstance Level4EndInstance;
        static SoundEffect Fall1;
        static SoundEffect Fall2;
        static SoundEffect FraisiMini;
        static SoundEffect FraisiMini2;




        public static void Load(ContentManager content)
        {
    
            Jump = content.Load<SoundEffect>("SoundEffects/Jump8");
            JumpInstance = Jump.CreateInstance();
            Straw = content.Load<SoundEffect>("SoundEffects/StrawTaken2");
            Leaf2 = content.Load<SoundEffect>("SoundEffects/Leaf2");
            Birds = content.Load<SoundEffect>("Intro/WingFlap01");
            EnterIn = content.Load<SoundEffect>("Intro/alert_3");
            EnterOut = content.Load<SoundEffect>("Intro/EnterPressed");
            LevelEnd4 = content.Load<SoundEffect>("SoundEffects/EndLevel4");
            Level4EndInstance = LevelEnd4.CreateInstance();
            Fall1 = content.Load<SoundEffect>("SoundEffects/Falling3");
            Fall2 = content.Load<SoundEffect>("SoundEffects/Falling2");
            FraisiMini = content.Load<SoundEffect>("SoundEffects/FraisiMini");
            FraisiMini2 = content.Load<SoundEffect>("SoundEffects/Fraisimini3");
            
    
        }




        public static void PlayJumpEffect()
        { JumpInstance.Play(); }
        
        public static void StopJumpEffect()
        { JumpInstance.Stop(); }
               
        public static void StrawEffect()
        { Straw.Play(); }

        public static void LeafEffect2()
        { Leaf2.Play(); }

        public static void BirdEffect()
        { Birds.Play(); }

        public static void EnterInEffect()
        { EnterIn.Play(); }

        public static void EnterOutEffect()
        { EnterOut.Play(); }

        public static void PlayLevelEndEffect()
        { Level4EndInstance.Play(); }

        public static void StopLevelEndEffect()
        { Level4EndInstance.Stop(); }

        public static void PlayFall1()
        { Fall1.Play(); }

        public static void PlayFall2()
        { Fall2.Play(); }

        public static void PlayFraisiMini()
        { FraisiMini.Play(); }

        public static void PlayFraisiMini2()
        { FraisiMini2.Play(); }
    }

}
