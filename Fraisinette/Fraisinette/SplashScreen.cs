﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fraisinette
{
    class SplashScreen
    {

        Texture2D Splash;
        Texture2D Entre;
        Texture2D LogoOverlay;
        Texture2D GazonOverlay;
        Texture2D FlyingBirds;

        Vector2 BirdsPosition = new Vector2(0, 0);

        float transparent = 0f;
        float fading = 1f;
        float interval = 80f;
        float timer = 0f;
        float timer2 = 0f;
        float timer3 = 0f;
        float wait = 3500f;

        bool max = false;
        bool go = false;
        bool bird = false;
        bool fade = false;

        public void Load(ContentManager content)
        {
            Splash = content.Load<Texture2D>("Intro/SplashScreen");
            Entre = content.Load<Texture2D>("Intro/Entrée");
            LogoOverlay = content.Load<Texture2D>("Intro/LogoOverlay");
            GazonOverlay = content.Load<Texture2D>("Intro/GazonOverlay");
            FlyingBirds = content.Load<Texture2D>("Intro/FlyingBirds");

        }
        
        public void Update(GameTime gameTime, KeyboardState ks)
        {
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);


            if (Music.musicState == Music.MusicState.notPlaying)
            {
                Music.musicState = Music.MusicState.nowPlaying;
                Music.Theme();

            }
            if (MediaPlayer.State == MediaState.Stopped)
                Music.musicState = Music.MusicState.notPlaying;
            
            
            timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            timer2 += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            timer3 += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            
            if (timer > wait)
            { go = true; SoundFx.EnterInEffect(); }
            if (timer2 > wait / 5)
            { bird = true; SoundFx.BirdEffect(); }

            //Birds animation
            if (bird)
            {
                if (timer2 > interval)
                {
                    timer2 = 0f;
                    BirdsPosition.X -= 11f;
                    BirdsPosition.Y -= 30F;
                }
            }

            //''enter'' animation
            if (go)
            {
                if (max == false)
                {
                    if (timer > interval)
                    {
                        timer = 0f;
                        transparent += 0.1f;
                        if (transparent > 1f)
                            max = true;
                    }
                }
                else if (max == true)
                {
                    if (timer > interval)
                    {
                        timer = 0f;
                        transparent -= 0.1f;
                        if (transparent < 0f)
                            max = false;
                    }
                }

            }

            //changement de state
            if ((go) && (ks.IsKeyDown(Keys.Enter) || (gamePadState.Buttons.A == ButtonState.Pressed)))
            { fade = true; SoundFx.EnterOutEffect(); go = false; }

            if (fade == true)
            {
                if (timer3 > interval)
                {
                    timer3 = 0f;
                    fading -= 0.1f;
                    MediaPlayer.Volume -= 0.025f;

                    if (fading < 0.1f)
                    {
                        GameState.state = GameState.State.worldMap;
                        Music.musicState = Music.MusicState.notPlaying;
                        MediaPlayer.Volume = Music.MatVolume;
                    }

                }
            }

        }
        public void Draw(SpriteBatch spriteBatch)
        {
            if (!fade)
            {
                spriteBatch.Draw(Splash, Vector2.Zero, Color.White);
                spriteBatch.Draw(FlyingBirds, BirdsPosition, Color.White * 0.4f);
                spriteBatch.Draw(LogoOverlay, Vector2.Zero, Color.White);
                spriteBatch.Draw(GazonOverlay, Vector2.Zero, Color.White);
                spriteBatch.Draw(Entre, Vector2.Zero, Color.White * transparent);
            }
            if (fade)
            {
                spriteBatch.Draw(Splash, Vector2.Zero, Color.White * fading);
                spriteBatch.Draw(FlyingBirds, BirdsPosition, Color.White * fading);
                spriteBatch.Draw(LogoOverlay, Vector2.Zero, Color.White * fading);
                spriteBatch.Draw(GazonOverlay, Vector2.Zero, Color.White * (fading / 8));
                spriteBatch.Draw(Entre, Vector2.Zero, Color.White * (fading / 4));
            }
        }
    }
}