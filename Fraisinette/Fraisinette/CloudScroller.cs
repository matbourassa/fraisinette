﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace Fraisinette
{
    class CloudScroller
    {

        Texture2D CloudTexture;
        Vector2 CloudPosition = new Vector2(0);
        Vector2 CloudPosition2 = new Vector2(4096, 0);
                
        public void LoadContent(ContentManager content)
        {
            CloudTexture = content.Load<Texture2D>("Level2/Cloud");
        }
        
        public void Update()
        {
            CloudPosition.X -= 0.25f;
            CloudPosition2.X -= 0.25f;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(CloudTexture, CloudPosition, Color.White);
            spriteBatch.Draw(CloudTexture, CloudPosition2, Color.White);
        }

    }
}
