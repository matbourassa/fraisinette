﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fraisinette
{
    class WorldMap
    {
        //GraphicsDevice graphicsDevice;
        //ContentManager content;
        
        Vector2 MapPosition =  new Vector2(0);
        Vector2 InstructionsPosition = new Vector2(-400, 485);
        Vector2 PersoPosition = new Vector2(290, 885);
        Vector2 PersoTarget = new Vector2(290, 885);
        Vector2 LevelDone1Position = new Vector2(740, 765);
        Vector2 LevelDone2Position = new Vector2(30, 510);
        Vector2 LevelDone3Position = new Vector2(605, 430);
                
        ParticleEngine MapParticle;
        
        Texture2D MapTexture;
        Texture2D InstructionsLevel1Texture;
        Texture2D InstructionsLevel2Texture;
        Texture2D InstructionsLevel3Texture;
        Texture2D InstructionsLevel4Texture;
        Texture2D InstructionsLevel5Texture;
        Texture2D InstructionsLevel6Texture;
        Texture2D PersoTexture;
        Texture2D LevelDoneTexture;
                
        enum InstructionsState {ShowLevel1, ShowLevel2, HideLevel1, HideLevel2, ShowLevel3, HideLevel3, ShowLevel4, HideLevel4, ShowLevel5, HideLevel5, ShowLevel6, HideLevel6};
        InstructionsState instructionsState = new InstructionsState();

        public enum PositionState { Start, Level1, Level2, Level3, Level4, Level5, Level6 };
        public static PositionState positionState = new PositionState();

        Queue<Vector2> WayPoints01 = new Queue<Vector2>();
        Queue<Vector2> WayPoints02 = new Queue<Vector2>();
        Queue<Vector2> WayPoints03 = new Queue<Vector2>();
        Queue<Vector2> WayPoints04 = new Queue<Vector2>();
        Queue<Vector2> WayPoints05 = new Queue<Vector2>();
        Queue<Vector2> WayPoints06 = new Queue<Vector2>();
        Queue<Vector2> WayPoints07 = new Queue<Vector2>();
        Queue<Vector2> WayPoints08 = new Queue<Vector2>();
        Queue<Vector2> WayPoints09 = new Queue<Vector2>();
        Queue<Vector2> WayPoints10 = new Queue<Vector2>();
        Queue<Vector2> WayPoints11 = new Queue<Vector2>();
        Queue<Vector2> WayPoints12 = new Queue<Vector2>();

        float Velocity = 10f;
        float PersoSpeed = 8f;

        bool GoForth = false, GoBack = false, GoLeft = false;
        bool ClavierOn = true;
        public static bool LevelDone1 = false; 
        public static bool LevelDone2 = false; 
        public static bool LevelDone3 = false; 
        public static bool LevelDone4 = false;
        public static bool LevelDone5 = false; 
        public static bool LevelDone6 = false;
        public static bool EnterLevel = false;
        public static bool EnterInstruction = false;

        public static bool BackFromLevel1 = false;
        public static bool BackFromLevel2 = false;
        public static bool BackFromLevel3 = false;

        Level1 level01;
        Level2 level02;
        Level3 level03;


        public WorldMap(Level1 level1, Level2 level2, Level3 level3)
        {
            level01 = level1;
            level02 = level2;
            level03 = level3;
                
        }

        
        public void Load(ContentManager content)
        {
            //Les textures
            InstructionsLevel1Texture = content.Load<Texture2D>("WorldMap/InstructionsLevel1");
            InstructionsLevel2Texture = content.Load<Texture2D>("WorldMap/InstructionsLevel2");
            InstructionsLevel3Texture = content.Load<Texture2D>("WorldMap/InstructionsLevel3");
            InstructionsLevel4Texture = content.Load<Texture2D>("WorldMap/InstructionsLevelTemp");
            InstructionsLevel5Texture = content.Load<Texture2D>("WorldMap/InstructionsLevelTemp");
            InstructionsLevel6Texture = content.Load<Texture2D>("WorldMap/InstructionsLevelTemp");
            MapTexture = content.Load<Texture2D>("WorldMap/WorldMap");
            PersoTexture = content.Load<Texture2D>("WorldMap/MapPerso");
            LevelDoneTexture = content.Load<Texture2D>("WorldMap/Check");
            
            //Les différents états
            
            positionState = PositionState.Start;
            instructionsState = InstructionsState.HideLevel1;

            //Charger le premier waypoint
            PathPoint01();
                                    
            //Particle**
            List<Texture2D> texture = new List<Texture2D>();
            texture.Add(content.Load<Texture2D>("Particles/CrossParticle"));
            texture.Add(content.Load<Texture2D>("Particles/Cross2Particle"));
            MapParticle = new ParticleEngine(texture, new Vector2(0));
        }


        public void Update(KeyboardState ks, ContentManager content, GraphicsDevice graphicsDevice)
        {
            
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);
            //Music
            if (Music.musicState == Music.MusicState.notPlaying)
            {
                Music.musicState = Music.MusicState.nowPlaying;
                Music.ThemeInstrument();

            }
            if (MediaPlayer.State == MediaState.Stopped)
                Music.musicState = Music.MusicState.notPlaying;

            ReInitialization(content, graphicsDevice);



            //WayPoints
            if (PersoPosition == new Vector2(290, 885))
            { positionState = PositionState.Start; ClavierOn = true; }
            if (PersoPosition == new Vector2(740, 765))
            { positionState = PositionState.Level1; ClavierOn = true; }
            if (PersoPosition == new Vector2(30, 510))
            { positionState = PositionState.Level2; ClavierOn = true; }
            if (PersoPosition == new Vector2(605, 430))
            { positionState = PositionState.Level3; ClavierOn = true; }
            if (PersoPosition == new Vector2(485, 200))
            { positionState = PositionState.Level4; ClavierOn = true; }
            if (PersoPosition == new Vector2(1250, 300))
            { positionState = PositionState.Level5; ClavierOn = true; }
            if (PersoPosition == new Vector2(1280, 745))
            { positionState = PositionState.Level6; ClavierOn = true; }

            if (ClavierOn)
            {
                if ((positionState == PositionState.Start) && (ks.IsKeyDown(Keys.Right) || (gamePadState.DPad.Right == ButtonState.Pressed)))
                { GoForth = true; GoBack = false; }
                if ((positionState == PositionState.Level1) && (ks.IsKeyDown(Keys.Down) || (gamePadState.DPad.Down == ButtonState.Pressed)))
                { GoBack = true; GoForth = false; }
                if ((positionState == PositionState.Level1) && (ks.IsKeyDown(Keys.Left) || (gamePadState.DPad.Left == ButtonState.Pressed)))
                { GoForth = true; GoBack = false; }
                if ((positionState == PositionState.Level2) && (ks.IsKeyDown(Keys.Down) || (gamePadState.DPad.Down == ButtonState.Pressed)))
                { GoBack = true; GoForth = false; }
                if ((positionState == PositionState.Level2) && (ks.IsKeyDown(Keys.Right) || (gamePadState.DPad.Right == ButtonState.Pressed)))
                { GoForth = true; GoBack = false; }
                if ((positionState == PositionState.Level3) && (ks.IsKeyDown(Keys.Left) || (gamePadState.DPad.Left == ButtonState.Pressed)))
                { GoBack = true; GoForth = false; GoLeft = false; }
                if ((positionState == PositionState.Level3) && (ks.IsKeyDown(Keys.Up) || (gamePadState.DPad.Up == ButtonState.Pressed)))
                { GoForth = true; GoBack = false; GoLeft = false; }
                if ((positionState == PositionState.Level4) && (ks.IsKeyDown(Keys.Down) || (gamePadState.DPad.Down == ButtonState.Pressed)))
                { GoBack = true; GoForth = false; }
                if ((positionState == PositionState.Level3) && (ks.IsKeyDown(Keys.Right) || (gamePadState.DPad.Right == ButtonState.Pressed)))
                { GoLeft = true; GoForth = false; GoBack = false; }
                if ((positionState == PositionState.Level5) && (ks.IsKeyDown(Keys.Left) || (gamePadState.DPad.Left == ButtonState.Pressed)))
                { GoBack = true; GoForth = false; }
                if ((positionState == PositionState.Level5) && (ks.IsKeyDown(Keys.Down) || (gamePadState.DPad.Down == ButtonState.Pressed)))
                { GoForth = true; GoBack = false; }
                if ((positionState == PositionState.Level6) && (ks.IsKeyDown(Keys.Down) || (gamePadState.DPad.Down == ButtonState.Pressed)))
                { GoBack = true; GoForth = false; }
            }

            if ((positionState == PositionState.Start) && (GoForth))
            { WayPointsUpdate(WayPoints01); }
            if ((positionState == PositionState.Level1) && (GoBack))
            { WayPointsUpdate(WayPoints02); }
            if ((positionState == PositionState.Level1) && (GoForth))
            { WayPointsUpdate(WayPoints03); }
            if ((positionState == PositionState.Level2) && (GoBack))
            { WayPointsUpdate(WayPoints04); }
            if ((positionState == PositionState.Level2) && (GoForth))
            { WayPointsUpdate(WayPoints05); }
            if ((positionState == PositionState.Level3) && (GoBack))
            { WayPointsUpdate(WayPoints06); }
            if ((positionState == PositionState.Level3) && (GoForth))
            { WayPointsUpdate(WayPoints07); }
            if ((positionState == PositionState.Level4) && (GoBack))
            { WayPointsUpdate(WayPoints08); }
            if ((positionState == PositionState.Level3) && (GoLeft))
            { WayPointsUpdate(WayPoints09); }
            if ((positionState == PositionState.Level5) && (GoBack))
            { WayPointsUpdate(WayPoints10); }
            if ((positionState == PositionState.Level5) && (GoForth))
            { WayPointsUpdate(WayPoints11); }
            if ((positionState == PositionState.Level6) && (GoBack))
            { WayPointsUpdate(WayPoints12); }

                        
          
            //Particles 
            Random random = new Random();
            MapParticle.EmitterLocation = new Vector2(PersoPosition.X + 32, PersoPosition.Y + 32);
            MapParticle.Update(30, 1f, 1f, 4, 4, (float)random.NextDouble(), (float)random.NextDouble(), (float)random.NextDouble(), true, 1, 2, new Vector2(0));
            
            
            
            //Entrer dans un niveau et montrer les instructions
            if (positionState == PositionState.Level1)
            {
                InstructionMove();

                if (ks.IsKeyDown(Keys.Enter) || (gamePadState.Buttons.Start == ButtonState.Pressed))
                {
                    EnterInstruction = true;
                    instructionsState = InstructionsState.ShowLevel1;


                    if (ks.IsKeyDown(Keys.Enter) || (gamePadState.Buttons.Start == ButtonState.Pressed) && EnterLevel)
                    {
                        EnterInstruction = false;
                        instructionsState = InstructionsState.HideLevel1;
                        Velocity = 0f;
                        Music.musicState = Music.MusicState.notPlaying;
                        GameState.state = GameState.State.level1;

                    }
                }
                if ((ks.IsKeyDown(Keys.Down)) || (gamePadState.DPad.Down == ButtonState.Pressed) || (ks.IsKeyDown(Keys.Left) || (gamePadState.DPad.Left == ButtonState.Pressed)))
                { EnterInstruction = false; instructionsState = InstructionsState.HideLevel1; }


            }

            if (positionState == PositionState.Level2)
            {
                InstructionMove();

                if (ks.IsKeyDown(Keys.Enter) || (gamePadState.Buttons.Start == ButtonState.Pressed))
                {
                    EnterInstruction = true;
                    instructionsState = InstructionsState.ShowLevel2;

                    if (ks.IsKeyDown(Keys.Enter) || (gamePadState.Buttons.Start == ButtonState.Pressed) && EnterLevel)
                    {
                        EnterInstruction = false;
                        instructionsState = InstructionsState.HideLevel2;
                        Velocity = 0f;
                        Music.musicState = Music.MusicState.notPlaying;
                        GameState.state = GameState.State.level2;
                    }
                }
                if ((ks.IsKeyDown(Keys.Down)) || (gamePadState.DPad.Down == ButtonState.Pressed) || (ks.IsKeyDown(Keys.Right) || (gamePadState.DPad.Right == ButtonState.Pressed)))
                { EnterInstruction = false; instructionsState = InstructionsState.HideLevel2; }
            }

            if (positionState == PositionState.Level3)
            {
                InstructionMove();

                if (ks.IsKeyDown(Keys.Enter) || (gamePadState.Buttons.Start == ButtonState.Pressed))
                {
                    EnterInstruction = true;
                    instructionsState = InstructionsState.ShowLevel3;

                    if (ks.IsKeyDown(Keys.Enter) || (gamePadState.Buttons.Start == ButtonState.Pressed) && EnterLevel)
                    {
                        EnterInstruction = false;
                        instructionsState = InstructionsState.HideLevel3;
                        Velocity = 0f;
                        Music.musicState = Music.MusicState.notPlaying;
                        GameState.state = GameState.State.level3;
                    }
                }
                if ((ks.IsKeyDown(Keys.Right)) || (gamePadState.DPad.Right == ButtonState.Pressed) || (ks.IsKeyDown(Keys.Left)) || (ks.IsKeyDown(Keys.Up) || (gamePadState.DPad.Up == ButtonState.Pressed)))
                { EnterInstruction = false; instructionsState = InstructionsState.HideLevel3; }
            }
            
            if (positionState == PositionState.Level4)
            {
                InstructionMove();

                if (ks.IsKeyDown(Keys.Enter) || (gamePadState.Buttons.Start == ButtonState.Pressed))
                {
                    EnterInstruction = true;
                    instructionsState = InstructionsState.ShowLevel4;

                    if (ks.IsKeyDown(Keys.Enter) || (gamePadState.Buttons.Start == ButtonState.Pressed) && EnterLevel)
                    {
                        EnterInstruction = false;
                        instructionsState = InstructionsState.HideLevel4;
                        Velocity = 0f;
                        Music.musicState = Music.MusicState.notPlaying;
                        //GameState.state = GameState.State.level4;
                    }
                }
                if ((ks.IsKeyDown(Keys.Down) || (gamePadState.DPad.Down == ButtonState.Pressed)))
                { EnterInstruction = false; instructionsState = InstructionsState.HideLevel4; }
            }

            if (positionState == PositionState.Level5)
            {
                InstructionMove();

                if (ks.IsKeyDown(Keys.Enter) || (gamePadState.Buttons.Start == ButtonState.Pressed))
                {
                    EnterInstruction = true;
                    instructionsState = InstructionsState.ShowLevel5;

                    if (ks.IsKeyDown(Keys.Enter) || (gamePadState.Buttons.Start == ButtonState.Pressed) && EnterLevel)
                    {
                        EnterInstruction = false;
                        instructionsState = InstructionsState.HideLevel5;
                        Velocity = 0f;
                        Music.musicState = Music.MusicState.notPlaying;
                        //GameState.state = GameState.State.level5;
                    }
                }
                if ((ks.IsKeyDown(Keys.Down)) || (gamePadState.DPad.Down == ButtonState.Pressed) || (ks.IsKeyDown(Keys.Left) || (gamePadState.DPad.Left == ButtonState.Pressed)))
                { EnterInstruction = false; instructionsState = InstructionsState.HideLevel5; }
            }
           
            if (positionState == PositionState.Level6)
            {
                InstructionMove();

                if (ks.IsKeyDown(Keys.Enter) || (gamePadState.Buttons.Start == ButtonState.Pressed))
                {
                    EnterInstruction = true;
                    instructionsState = InstructionsState.ShowLevel6;

                    if (ks.IsKeyDown(Keys.Enter) || (gamePadState.Buttons.Start == ButtonState.Pressed) && EnterLevel)
                    {
                        EnterInstruction = false;
                        instructionsState = InstructionsState.HideLevel6;
                        Velocity = 0f;
                        Music.musicState = Music.MusicState.notPlaying;
                        //GameState.state = GameState.State.level6;
                    }
                }
            }
            if ((ks.IsKeyDown(Keys.Down) || (gamePadState.DPad.Down == ButtonState.Pressed)))
            { EnterInstruction = false; instructionsState = InstructionsState.HideLevel6; }
        }



        public void InstructionMove()
        {
            

            if (EnterInstruction)
            {
                InstructionsPosition.Y += Velocity;
                if (InstructionsPosition.Y < 20)
                { InstructionsPosition.Y = 20; EnterLevel = true; }
                if ((instructionsState == InstructionsState.ShowLevel1) || (instructionsState == InstructionsState.ShowLevel2) || (instructionsState == InstructionsState.ShowLevel3)
                   || (instructionsState == InstructionsState.ShowLevel4) || (instructionsState == InstructionsState.ShowLevel5) || (instructionsState == InstructionsState.ShowLevel6))
                { Velocity = -25f; }
            }

            if (!EnterInstruction)
            {
                InstructionsPosition.Y += Velocity; 
                if (InstructionsPosition.Y > 485)
                { InstructionsPosition.Y = 485; EnterLevel = false; }

                if ((instructionsState == InstructionsState.HideLevel1) || (instructionsState == InstructionsState.HideLevel2) || (instructionsState == InstructionsState.HideLevel3)
                    || (instructionsState == InstructionsState.HideLevel4) || (instructionsState == InstructionsState.HideLevel5) || (instructionsState == InstructionsState.HideLevel6))
                { Velocity = 25f; }
            }  
        }        
                             
                  
             
        //Calculer le déplacement du curseur sur la carte
        public void WayPointsUpdate(Queue<Vector2> points)
        {
            
            
            if ((GoForth) || (GoBack) || (GoLeft))
            {
                if (instructionsState == InstructionsState.ShowLevel1)
                    instructionsState = InstructionsState.HideLevel1;
                if (instructionsState == InstructionsState.ShowLevel2)
                    instructionsState = InstructionsState.HideLevel2;
                if (instructionsState == InstructionsState.ShowLevel3)
                    instructionsState = InstructionsState.HideLevel3;
                if (instructionsState == InstructionsState.ShowLevel4)
                    instructionsState = InstructionsState.HideLevel4;
                if (instructionsState == InstructionsState.ShowLevel5)
                    instructionsState = InstructionsState.HideLevel5;
                if (instructionsState == InstructionsState.ShowLevel6)
                    instructionsState = InstructionsState.HideLevel6;
                
                
                ClavierOn = false;
                 
                Vector2 Delta = new Vector2(PersoTarget.X - PersoPosition.X, PersoTarget.Y - PersoPosition.Y);

                if (Delta.Length() > PersoSpeed)
                {
                    Delta.Normalize();
                    Delta *= PersoSpeed;
                    PersoPosition += Delta;
                }
                else
                {
                    if (points.Count > 0)
                    {
                        PersoTarget = points.Dequeue();
                    }
                    else
                    {   
                        PersoPosition = PersoTarget;
                        
                        if ((positionState == PositionState.Start))
                        { WayPoints01.Clear(); PathPoint01(); GoForth = false; InstructionMove(); }
                        if ((positionState == PositionState.Level1) || (GoBack == true))
                        {   WayPoints02.Clear(); PathPoint02(); GoBack = false; }
                        if ((positionState == PositionState.Level1) || (GoForth == true))
                        { WayPoints03.Clear(); PathPoint03(); GoForth = false; }
                        if ((positionState == PositionState.Level2) || (GoBack == true))
                        { WayPoints04.Clear(); PathPoint04(); GoBack = false;  }
                        if ((positionState == PositionState.Level2) || (GoForth == true))
                        { WayPoints05.Clear(); PathPoint05(); GoForth = false;  }
                        if ((positionState == PositionState.Level3) || (GoBack == true))
                        { WayPoints06.Clear(); PathPoint06(); GoBack = false; }
                        if ((positionState == PositionState.Level3) || (GoForth == true))
                        { WayPoints07.Clear(); PathPoint07(); GoForth = false; }
                        if ((positionState == PositionState.Level4) || (GoBack == true))
                        { WayPoints08.Clear(); PathPoint08(); GoBack = false; }
                        if ((positionState == PositionState.Level3) || (GoLeft == true))
                        { WayPoints09.Clear(); PathPoint09(); GoLeft = false; }
                        if ((positionState == PositionState.Level5) || (GoBack == true))
                        { WayPoints10.Clear(); PathPoint10(); GoBack = false; }
                        if ((positionState == PositionState.Level5) || (GoForth == true))
                        { WayPoints11.Clear(); PathPoint11(); GoForth = false; }
                        if ((positionState == PositionState.Level6) || (GoBack == true))
                        { WayPoints12.Clear(); PathPoint12(); GoBack = false; }

                    }
                }
                
            }
            
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(MapTexture, MapPosition, Color.White);

            //Level qui sont complétés
            if (LevelDone1)
                spriteBatch.Draw(LevelDoneTexture, LevelDone1Position, Color.White);
            if (LevelDone2)
                spriteBatch.Draw(LevelDoneTexture, LevelDone2Position, Color.White);
            if (LevelDone3)
                spriteBatch.Draw(LevelDoneTexture, LevelDone3Position, Color.White);
            

            //Personnage
            MapParticle.Draw(spriteBatch);
            spriteBatch.Draw(PersoTexture, PersoPosition, Color.White);
            
            
            //Instructions
            
            if ((instructionsState == InstructionsState.ShowLevel1) || (instructionsState == InstructionsState.HideLevel1))
               spriteBatch.Draw(InstructionsLevel1Texture, InstructionsPosition, Color.White);
            
            if ((instructionsState == InstructionsState.ShowLevel2) || (instructionsState == InstructionsState.HideLevel2))
                spriteBatch.Draw(InstructionsLevel2Texture, InstructionsPosition, Color.White);

            if ((instructionsState == InstructionsState.ShowLevel3) || (instructionsState == InstructionsState.HideLevel3))
                spriteBatch.Draw(InstructionsLevel3Texture, InstructionsPosition, Color.White);

            if ((instructionsState == InstructionsState.ShowLevel4) || (instructionsState == InstructionsState.HideLevel4))
                spriteBatch.Draw(InstructionsLevel4Texture, InstructionsPosition, Color.White);

            if ((instructionsState == InstructionsState.ShowLevel5) || (instructionsState == InstructionsState.HideLevel5))
                spriteBatch.Draw(InstructionsLevel5Texture, InstructionsPosition, Color.White);
            
            if ((instructionsState == InstructionsState.ShowLevel6) || (instructionsState == InstructionsState.HideLevel6))
                spriteBatch.Draw(InstructionsLevel6Texture, InstructionsPosition, Color.White);
        }



        public void ReInitialization(ContentManager content, GraphicsDevice graphicsDevice)
        {
            if (BackFromLevel1)
            {
                level01.map1.CollisionTiles.Clear(); level01.map1.CollectionTiles.Clear(); level01.map1.DecorationTiles.Clear(); level01.map1.EvenementTiles.Clear(); 
                level01.Initialize();
                level01.Load(content, graphicsDevice);
                BackFromLevel1 = false;
            };

            if (BackFromLevel2)
            {
                level02.map2.CollisionTiles.Clear(); level02.map2.CollectionTiles.Clear(); level02.map2.DecorationTiles.Clear(); level02.map2.EvenementTiles.Clear();
                level02.map2back.CollisionTiles.Clear(); level02.map2back.CollectionTiles.Clear(); level02.map2back.DecorationTiles.Clear(); level02.map2back.EvenementTiles.Clear();
                level02.Initialize();
                level02.Load(content, graphicsDevice);
                BackFromLevel2 = false;
            };

            if (BackFromLevel3)
            {
                level03.map3.CollisionTiles.Clear(); level02.map2.CollectionTiles.Clear(); level02.map2.DecorationTiles.Clear(); level02.map2.EvenementTiles.Clear();
                level03.Initialize();
                level03.Load(content, graphicsDevice);
                BackFromLevel3 = false;
            };

        }



        public void PathPoint01()
        {
            WayPoints01.Enqueue(new Vector2(365, 905));
            WayPoints01.Enqueue(new Vector2(445, 905));
            WayPoints01.Enqueue(new Vector2(520, 895));
            WayPoints01.Enqueue(new Vector2(600, 880));
            WayPoints01.Enqueue(new Vector2(675, 860));
            WayPoints01.Enqueue(new Vector2(730, 800));
            WayPoints01.Enqueue(new Vector2(740, 765));
        }

        public void PathPoint02()
        {
            WayPoints02.Enqueue(new Vector2(730, 800));
            WayPoints02.Enqueue(new Vector2(675, 860));
            WayPoints02.Enqueue(new Vector2(600, 880));
            WayPoints02.Enqueue(new Vector2(520, 895));
            WayPoints02.Enqueue(new Vector2(445, 905));
            WayPoints02.Enqueue(new Vector2(365, 905));
            WayPoints02.Enqueue(new Vector2(290, 885));
        }

        public void PathPoint03()
        {
            WayPoints03.Enqueue(new Vector2(660, 770));
            WayPoints03.Enqueue(new Vector2(585, 770));
            WayPoints03.Enqueue(new Vector2(510, 740));
            WayPoints03.Enqueue(new Vector2(445, 700));
            WayPoints03.Enqueue(new Vector2(380, 660));
            WayPoints03.Enqueue(new Vector2(315, 610));
            WayPoints03.Enqueue(new Vector2(250, 570));
            WayPoints03.Enqueue(new Vector2(175, 550));
            WayPoints03.Enqueue(new Vector2(100, 550));
            WayPoints03.Enqueue(new Vector2(20, 545));
            WayPoints03.Enqueue(new Vector2(30, 510));
        }

        public void PathPoint04()
        {
            WayPoints04.Enqueue(new Vector2(20, 545));
            WayPoints04.Enqueue(new Vector2(100, 550));
            WayPoints04.Enqueue(new Vector2(175, 550));
            WayPoints04.Enqueue(new Vector2(250, 570));
            WayPoints04.Enqueue(new Vector2(315, 610));
            WayPoints04.Enqueue(new Vector2(380, 660));
            WayPoints04.Enqueue(new Vector2(445, 700));
            WayPoints04.Enqueue(new Vector2(510, 740));
            WayPoints04.Enqueue(new Vector2(585, 770));
            WayPoints04.Enqueue(new Vector2(660, 770));
            WayPoints04.Enqueue(new Vector2(740, 765));
        }

        public void PathPoint05()
        {
            WayPoints05.Enqueue(new Vector2(20, 545));
            WayPoints05.Enqueue(new Vector2(100, 550));
            WayPoints05.Enqueue(new Vector2(175, 550));
            WayPoints05.Enqueue(new Vector2(235, 535));
            WayPoints05.Enqueue(new Vector2(315, 525));
            WayPoints05.Enqueue(new Vector2(390, 515));
            WayPoints05.Enqueue(new Vector2(470, 495));
            WayPoints05.Enqueue(new Vector2(545, 475));
            WayPoints05.Enqueue(new Vector2(605, 430));
        }

        public void PathPoint06()
        {
            WayPoints06.Enqueue(new Vector2(545, 475));
            WayPoints06.Enqueue(new Vector2(470, 495));
            WayPoints06.Enqueue(new Vector2(390, 515));
            WayPoints06.Enqueue(new Vector2(315, 525));
            WayPoints06.Enqueue(new Vector2(235, 535));
            WayPoints06.Enqueue(new Vector2(175, 550));
            WayPoints06.Enqueue(new Vector2(100, 550));
            WayPoints06.Enqueue(new Vector2(20, 545));
            WayPoints06.Enqueue(new Vector2(30, 510));
        }

        public void PathPoint07()
        {
            WayPoints07.Enqueue(new Vector2(535, 415));
            WayPoints07.Enqueue(new Vector2(470, 370));
            WayPoints07.Enqueue(new Vector2(435, 305));
            WayPoints07.Enqueue(new Vector2(480, 250));
            WayPoints07.Enqueue(new Vector2(485, 200));
           
        }

        public void PathPoint08()
        {
            WayPoints08.Enqueue(new Vector2(480, 250));
            WayPoints08.Enqueue(new Vector2(435, 305));
            WayPoints08.Enqueue(new Vector2(470, 370));
            WayPoints08.Enqueue(new Vector2(535, 415));
            WayPoints08.Enqueue(new Vector2(605, 430));
            
        }

        public void PathPoint09()
        {
            WayPoints09.Enqueue(new Vector2(695, 465));
            WayPoints09.Enqueue(new Vector2(775, 470));
            WayPoints09.Enqueue(new Vector2(855, 475));
            WayPoints09.Enqueue(new Vector2(930, 475));
            WayPoints09.Enqueue(new Vector2(1005, 470));
            WayPoints09.Enqueue(new Vector2(1080, 440));
            WayPoints09.Enqueue(new Vector2(1140, 395));
            WayPoints09.Enqueue(new Vector2(1200, 350));
            WayPoints09.Enqueue(new Vector2(1250, 300));
        }

        public void PathPoint10()
        {
            WayPoints10.Enqueue(new Vector2(1200, 350));
            WayPoints10.Enqueue(new Vector2(1140, 395));
            WayPoints10.Enqueue(new Vector2(1080, 440));
            WayPoints10.Enqueue(new Vector2(1005, 470));
            WayPoints10.Enqueue(new Vector2(930, 475));
            WayPoints10.Enqueue(new Vector2(855, 475));
            WayPoints10.Enqueue(new Vector2(775, 470));
            WayPoints10.Enqueue(new Vector2(695, 465));
            WayPoints10.Enqueue(new Vector2(605, 430));
        }

        public void PathPoint11()
        {
            WayPoints11.Enqueue(new Vector2(1200, 350));
            WayPoints11.Enqueue(new Vector2(1210, 405));
            WayPoints11.Enqueue(new Vector2(1215, 480));
            WayPoints11.Enqueue(new Vector2(1220, 560));
            WayPoints11.Enqueue(new Vector2(1185, 625));
            WayPoints11.Enqueue(new Vector2(1125, 675));
            WayPoints11.Enqueue(new Vector2(1075, 730));
            WayPoints11.Enqueue(new Vector2(1120, 790));
            WayPoints11.Enqueue(new Vector2(1200, 795));
            WayPoints11.Enqueue(new Vector2(1280, 745));
        }

        public void PathPoint12()
        {
            WayPoints12.Enqueue(new Vector2(1200, 795));
            WayPoints12.Enqueue(new Vector2(1120, 790));
            WayPoints12.Enqueue(new Vector2(1075, 730));
            WayPoints12.Enqueue(new Vector2(1125, 675));
            WayPoints12.Enqueue(new Vector2(1185, 625));
            WayPoints12.Enqueue(new Vector2(1220, 560));
            WayPoints12.Enqueue(new Vector2(1215, 480));
            WayPoints12.Enqueue(new Vector2(1210, 405));
            WayPoints12.Enqueue(new Vector2(1200, 350));
            WayPoints12.Enqueue(new Vector2(1250, 300));
        }
    }
}
