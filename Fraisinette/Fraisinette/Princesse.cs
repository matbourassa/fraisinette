﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fraisinette
{
    class Fraisinette : Sprite
    {

        float timer = 0f;
        float interval = 60f;
        

        float gravity = -0.5f;
        Vector2 velocity;
        float plancherY = 576;
        

        int currentframeRight = 8;
        int currentframeLeft = 0;
        int jumpframeRight = 17;
        int jumpframeLeft = 16;
        int spriteHeight = 128;
        int spriteWidth = 64;

        bool input = true;

        public Rectangle frameRectangleRight;
        public Rectangle frameRectangleLeft;
        public Rectangle frameRectangleJumpRight;
        public Rectangle frameRectangleJumpLeft;


        new public Vector2 SpritePosition
        { get { return spritePosition; } }



        private enum PersonnageState { standingRight, standingLeft, walkingRight, walkingLeft, jumpingRight, jumpingLeft, movingRight, movingLeft };
        PersonnageState personnageState = new PersonnageState();
        

        

        //********************************************************************************************



        public override void Initialize()
        {
            spritePosition = new Vector2(400, 576);
            personnageState = PersonnageState.standingRight;
            velocity = new Vector2(0,0);
        }


        public override void LoadContent(ContentManager content, string assetName)
        { spriteTexture = content.Load<Texture2D>("FraisinetteSheet"); }



        /***************************************************************************************************************************/

        public override void Update(GameTime gametime, GraphicsDevice graphicsDevice)
        {

            

            timer += (float)gametime.ElapsedGameTime.TotalMilliseconds;
            //spritePosition += velocity;
            spriteRectangle = new Rectangle((int)spritePosition.X, (int)spritePosition.Y, spriteWidth, spriteHeight);
            
           
            

            
            frameRectangleRight = new Rectangle(currentframeRight * spriteWidth, 0, spriteWidth, spriteHeight);
            frameRectangleLeft = new Rectangle(currentframeLeft * spriteWidth, 0, spriteWidth, spriteHeight);
            frameRectangleJumpRight = new Rectangle(jumpframeRight * spriteWidth, 0, spriteWidth, spriteHeight);
            frameRectangleJumpLeft = new Rectangle(jumpframeLeft * spriteWidth, 0, spriteWidth, spriteHeight);

            
            
            //Input de l'état de Fraisinette//////////////////////////////////////////////////////
            if (input == true)
            {
                KeyboardState keystate = Keyboard.GetState();


                //Marcher
                if (keystate.IsKeyDown(Keys.Right) && (personnageState != PersonnageState.movingRight))
                { personnageState = PersonnageState.walkingRight;}

                if (keystate.IsKeyDown(Keys.Left) && (personnageState != PersonnageState.movingLeft))
                { personnageState = PersonnageState.walkingLeft;}

                

                //Sauter sur place
                if (keystate.IsKeyDown(Keys.Space) && (personnageState == PersonnageState.standingRight))
                { personnageState = PersonnageState.jumpingRight; velocity.Y = 200f; }

                if (keystate.IsKeyDown(Keys.Space) && (personnageState == PersonnageState.standingLeft))
                { personnageState = PersonnageState.jumpingLeft; velocity.Y = 200f; }


                //Sauter en marchant


                if (keystate.IsKeyDown(Keys.Space) && (personnageState == PersonnageState.walkingRight))
                { personnageState = PersonnageState.movingRight; velocity.Y = 200; velocity.X = 5; }


                if (keystate.IsKeyDown(Keys.Space) && (personnageState == PersonnageState.walkingLeft))
                { personnageState = PersonnageState.movingLeft; velocity.Y = 200; velocity.X = 5; }


                

            }
            

            
            //Calculs animation et mouvement de Fraisinette///////////////////////////////////////////////////////////////////////////

            
            //Marcher
            if (personnageState == PersonnageState.walkingRight)
            {
                velocity.X = 6;
                spritePosition.X += velocity.X;
                if (timer > interval)
                {
                    currentframeRight++;
                    timer = 0;
                }

                if (currentframeRight == 12)
                {
                    currentframeRight = 8;
                    personnageState = PersonnageState.standingRight;
                    
                }
            }


            if (personnageState == PersonnageState.walkingLeft)
            {
                velocity.X = 6;
                spritePosition.X -= velocity.X;
                if (timer > interval)
                {
                    currentframeLeft++;
                    timer = 0;
                }

                if (currentframeLeft == 4)
                {
                    currentframeLeft = 0;
                    personnageState = PersonnageState.standingLeft;
                    
                }
            }


            //Sauter sur place
            if (personnageState == PersonnageState.jumpingRight)
            {
                input = false;
                spritePosition.Y -= velocity.Y;
                velocity.Y += gravity;



                if (velocity.Y == 0f)//spritePosition.Y > plancherY)
                {
                    //velocity.Y = 0; spritePosition.Y = plancherY;
                    personnageState = PersonnageState.standingRight;
                    input = true;
                }
             }


            if (personnageState == PersonnageState.jumpingLeft)
            {
                input = false;
                spritePosition.Y -= velocity.Y;
                velocity.Y += gravity;


                if (spritePosition.Y > plancherY)
                {
                    velocity.Y = 0; spritePosition.Y = plancherY;
                    personnageState = PersonnageState.standingLeft;
                    input = true;
                }
             }


            //Sauter en marchant


            if (personnageState == PersonnageState.movingRight)
            {
                input = false;
                 
                spritePosition.X += velocity.X;
                spritePosition.Y -= velocity.Y;
                velocity.Y += gravity;

                if (spritePosition.Y > plancherY)
                {
                    velocity.Y = 0; spritePosition.Y = plancherY;
                    personnageState = PersonnageState.standingRight;
                    velocity.X = 0;
                    input = true;
                    
                }
                    
            }

            if (personnageState == PersonnageState.movingLeft)
            {

                input = false;
                 
                spritePosition.X -= velocity.X;
                spritePosition.Y -= velocity.Y;
                velocity.Y += gravity;


                if (spritePosition.Y > plancherY)
                {
                    velocity.Y = 0; spritePosition.Y = plancherY;
                    personnageState = PersonnageState.standingLeft;
                    velocity.X = 0;
                    input = true;
                    
                }

            }



        }
             

        /********************* D  ***** R    ******* A *********** w    ***********************************************************/

        public override void Draw(SpriteBatch spriteBatch, GameTime gametime)
        {
            //Walking
            if (personnageState == PersonnageState.standingRight)
                spriteBatch.Draw(spriteTexture, spritePosition, frameRectangleRight, Color.White);


            if (personnageState == PersonnageState.standingLeft)
                spriteBatch.Draw(spriteTexture, spritePosition, frameRectangleLeft, Color.White);


            if (personnageState == PersonnageState.walkingRight)
                spriteBatch.Draw(spriteTexture, spritePosition, frameRectangleRight, Color.White);


            if (personnageState == PersonnageState.walkingLeft)
                spriteBatch.Draw(spriteTexture, spritePosition, frameRectangleLeft, Color.White);



            //Jumping
            if (personnageState == PersonnageState.jumpingRight)
                spriteBatch.Draw(spriteTexture, spritePosition, frameRectangleJumpRight, Color.White);


            if (personnageState == PersonnageState.jumpingLeft)
                spriteBatch.Draw(spriteTexture, spritePosition, frameRectangleJumpLeft, Color.White);

            if (personnageState == PersonnageState.movingRight)
                spriteBatch.Draw(spriteTexture, spritePosition, frameRectangleJumpRight, Color.White);

            if (personnageState == PersonnageState.movingLeft)
                spriteBatch.Draw(spriteTexture, spritePosition, frameRectangleJumpLeft, Color.White);
        }






        public void Collision(Rectangle newRectangle, int xOffSet, int yOffSet)
        { 
            if(spriteRectangle.TouchTopOf(newRectangle))
            {
                spriteRectangle.Y = newRectangle.Y - spriteRectangle.Height;
                velocity.Y = 0f;

            }


            if (spriteRectangle.TouchLeftOf(newRectangle))
            {

                spritePosition.X = newRectangle.X - spriteRectangle.Width - 6;

            }

            if (spriteRectangle.TouchRightOf(newRectangle))
            {
                spritePosition.X = newRectangle.X + newRectangle.Width + 6;
            
            }

            if (spriteRectangle.TouchBottomOf(newRectangle))
            {

                velocity.Y = 1f;
            }


            if (spritePosition.X < 0) spritePosition.X = 0;
            if (spritePosition.X > xOffSet - spriteRectangle.Width) spritePosition.X = xOffSet - spriteRectangle.Width;
            if (SpritePosition.Y < 0) velocity.Y = 1f;
            if (SpritePosition.Y > yOffSet - spriteRectangle.Height) spritePosition.Y = yOffSet - spriteRectangle.Height;



        }



    }

}