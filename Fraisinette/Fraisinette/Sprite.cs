﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Fraisinette
{
    class Sprite 
    {

        
                //TEXTURE
        public Texture2D SpriteTexture
        {
            get { return spriteTexture; }
            set { spriteTexture = value; }
        }
        public Texture2D spriteTexture;


            
        //POSITION
        public Vector2 SpritePosition
        {
            get { return spritePosition; }
            set { spritePosition = value; }
        }
        public Vector2 spritePosition;


        //DIRECTION       
        public Vector2 SpriteDirection
        {
            get { return spriteDirection; }
            set { spriteDirection = Vector2.Normalize(value); }
        }
        public Vector2 spriteDirection;

        
        //VITESSE
        public Vector2 SpriteSpeed
        {
            get { return spriteSpeed; }
            set { spriteSpeed = value; }
        }
        public Vector2 spriteSpeed;


        public Rectangle spriteRectangle;
        public Rectangle SpriteRectangle
        {
            get
            {
                return new Rectangle((int)spritePosition.X, (int)spritePosition.Y, spriteTexture.Width, spriteTexture.Height);
            }
        }
       
        //***************************************I  N  I  T  A  L  I  Z  E  ************************************************

        public virtual void Initialize()
        {
            spritePosition = Vector2.Zero;
            spriteDirection = Vector2.Zero;
            spriteSpeed = Vector2.Zero;
           
            
        }


        //************************************  L  O  A  D  ***************************************************************

        public virtual void LoadContent(ContentManager Content)
        {
           //spriteTexture = Content.Load<Texture2D>("");
        }


        //***************************************   U  P  D  A  T  E  *****************************************
        public virtual void Update(GameTime gameTime, GraphicsDevice graphicsDevice)
        {
                 
        }

        //**************************************  D  R  A  W  **************************************************************


        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
                   
            spriteBatch.Draw(spriteTexture, spritePosition, Color.White);
             
        }

        
    }



}

    



    




