﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace Fraisinette
{
    class PlayerTest
    {

        private Texture2D texture;
        private Vector2 position = new Vector2(400, 176);
        private Vector2 velocity;
        private Rectangle rectangle;

        private bool hasJump = false;

        public Vector2 Position
        { 
        
            get {return position;}
        
        }


        public PlayerTest() { }


        public void Load(ContentManager Content)
        {
            texture = Content.Load<Texture2D>("Tiles/Tile1");
        
        }



        public void Update(GameTime gameTime)
        {
            
            position += velocity;
            rectangle = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);

            Input(gameTime);


            if (velocity.Y < 10)
                velocity.Y += 0.4f;

           
            
        }
  
        private void Input(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                velocity.X = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 3;  
                
                
            }

            else if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                velocity.X = -(float)gameTime.ElapsedGameTime.TotalMilliseconds / 3;  
                
            }

            
            else velocity.X = 0f;


            if (Keyboard.GetState().IsKeyDown(Keys.Space) && hasJump == false)
            {

                position.Y -= 5f;
                velocity.Y = -8f;
                hasJump = true;
            }

            
                

                    
        }




        public void Collision(Rectangle newRectangle, int xOffSet, int yOffSet)
        {

            

            if (rectangle.TouchTopOf(newRectangle))  
            {
                rectangle.Y = newRectangle.Y - rectangle.Height;
                velocity.Y = 0f;
                hasJump = false;

            }


            if (rectangle.TouchLeftOf(newRectangle))  
            {

                position.X = newRectangle.X - rectangle.Width - 1;

            }

            if (rectangle.TouchRightOf(newRectangle)) 
            {
                position.X = newRectangle.X + newRectangle.Width + 1;

            }

            if (rectangle.TouchBottomOf(newRectangle)) 
            {

                velocity.Y = 1f;
            }


            if (position.X < 0) position.X = 0;
            if (position.X > xOffSet - rectangle.Width) position.X = xOffSet - rectangle.Width;
            if (position.Y < 0) velocity.Y = 1f;
            if (position.Y > yOffSet - rectangle.Height) position.Y = yOffSet - rectangle.Height;



        }


        public void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(texture, rectangle, Color.White);
        
        
        }







        }






    }

