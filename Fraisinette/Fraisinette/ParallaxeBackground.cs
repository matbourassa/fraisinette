﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;


namespace Fraisinette
{
    class ParallaxeBackground
    {
        Vector2 positionMiddleGround;
        Vector2 positionMiddleGround2;
        Vector2 positionFarGround = new Vector2(0, -150);
        Vector2 positionMontainBackGround;
        Vector2 positionGrassBackground;
        Vector2 positionGrassBackground2;
        
        Texture2D MiddleGround;
        Texture2D MiddleGround2;
        Texture2D FarGround;
        Texture2D MontainBackground;
        Texture2D GrassBackground;
        Texture2D GrassBackground2;
        
       public void Load(ContentManager content)
        {
            MiddleGround = content.Load<Texture2D>("Backgrounds/MiddleGroundLevel1");
            MiddleGround2 = content.Load<Texture2D>("Backgrounds/MiddleGroundLevel1");
            FarGround = content.Load<Texture2D>("Backgrounds/SkyBackground");
            MontainBackground = content.Load<Texture2D>("Backgrounds/MontainBackGround");
            GrassBackground = content.Load<Texture2D>("Backgrounds/GrassBackgournd");
            GrassBackground2 = content.Load<Texture2D>("Backgrounds/GrassBackgournd");
            positionGrassBackground.X = -337;
            positionGrassBackground2.X = 2500;
            positionMontainBackGround.X = -167;
            positionMiddleGround.X = -507;
            positionMiddleGround2.X = 2000;
        }

        
        public void Update(Vector2 playerPosition)
        {

            positionMiddleGround.Y = -playerPosition.Y * 0.6f + 378;
            positionMiddleGround2.Y = -playerPosition.Y * 0.6f + 378;
            positionGrassBackground.Y = -playerPosition.Y * 0.4f + 208;
            positionGrassBackground2.Y = -playerPosition.Y * 0.4f + 208;
            positionMontainBackGround.Y = playerPosition.Y * 0.2f - 168;
            
            if ((playerPosition.X > 850) && (playerPosition.X < 9600))
            {
                positionMiddleGround.X = -playerPosition.X * 0.6f;
                positionMiddleGround2.X = -playerPosition.X * 0.6f + 4000;
                positionGrassBackground.X = -playerPosition.X * 0.4f;
                positionGrassBackground2.X = -playerPosition.X * 0.4f + 4000;
                positionMontainBackGround.X = -playerPosition.X * 0.2f;
                
            }
         }
              
        
        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(FarGround, positionFarGround, Color.White);
            spritebatch.Draw(MontainBackground, positionMontainBackGround, Color.White);
            spritebatch.Draw(GrassBackground, positionGrassBackground, Color.White);
            spritebatch.Draw(GrassBackground2, positionGrassBackground2, Color.White);
            spritebatch.Draw(MiddleGround, positionMiddleGround, Color.White);
            spritebatch.Draw(MiddleGround2, positionMiddleGround2, Color.White);
             
        }


    }
}
