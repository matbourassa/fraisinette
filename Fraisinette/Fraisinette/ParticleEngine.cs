﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Fraisinette
{
    public class ParticleEngine
    {

        
        public Vector2 EmitterLocation { get; set; }
        
        
        
        private List<Particle> particles;
        private List<Texture2D> textures;
        private Random random;


        public ParticleEngine(List<Texture2D> textures, Vector2 location)
        {
            EmitterLocation = location;
            this.textures = textures;
            this.particles = new List<Particle>();
            random = new Random();
        }

        
        private Particle GenerateNewParticle(float velocityX, float velocityY, int velocityXmulti, int velocityYmulti, float colorR, 
            float colorG, float colorB, bool sizeBool, float scale, int death, Vector2 gravity)
        {
            Texture2D texture = textures[random.Next(textures.Count)];
            
            Vector2 position = EmitterLocation;
            
            Vector2 velocity = new Vector2(
                    velocityX * (float)(random.NextDouble() * velocityXmulti - 1),
                    velocityY * (float)(random.NextDouble() * velocityYmulti - 1));

                        
            float angle = 0;
            
            float angularVelocity = 0.1f * (float)(random.NextDouble() * 2 - 1);
            
            Color color = new Color(
                    colorR,
                    colorG,
                    colorB);

            float size;
            if (sizeBool)
            size = (float)random.NextDouble(); 
            else
            size = scale;
            
            int ttl = death + random.Next(40);

            return new Particle(texture, position, velocity, gravity, angle, angularVelocity, color, size, ttl);
        }


        public void Update(int total, float velocityX, float velocityY, int velocityXmulti, int velocityYmulti, float colorR,
            float colorG, float colorB, bool sizeBool, float scale, int death, Vector2 gravity)
        {
            
            for (int i = 0; i < total; i++)
            {
                particles.Add(GenerateNewParticle(velocityX, velocityY, velocityXmulti, velocityYmulti, colorR, colorG, colorB, sizeBool, scale, death, gravity));
                
                
            }

            for (int particle = 0; particle < particles.Count; particle++)
            {
                particles[particle].Update();
                if (particles[particle].TTL <= 0)
                {
                    particles.RemoveAt(particle);
                    particle--;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
             
            for (int index = 0; index < particles.Count; index++)
            {
                
                particles[index].Draw(spriteBatch);
            }
            
        }






    }
}
