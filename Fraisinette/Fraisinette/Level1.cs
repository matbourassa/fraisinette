﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fraisinette
{
    class Level1
    {

        public Map map1 = new Map();
        Player1 player1 = new Player1();
        ParallaxeBackground parallaxeBackground = new ParallaxeBackground();
        Camera camera;
        StrawCount strawCount = new StrawCount();
        ParticleEngine particleEngine;
        LevelEnd levelEnd = new LevelEnd();
        
        
        bool particle = false;
        float timer = 0f;



        public void Initialize()
        {
            player1.Initialize();
            

        
        }



        public void Load(ContentManager Content, GraphicsDevice graphicsDevice)
        {
            camera = new Camera(graphicsDevice.Viewport);
            Tiles.content = Content;
            map1.LoadLevel1();
            player1.Load(Content);
            strawCount.Load(Content, 1);
            parallaxeBackground.Load(Content);
            levelEnd.LoadContent(Content);
            
             
                
        }

        


        public void Update(ContentManager Content, GameTime gameTime, KeyboardState ks)
        {

            timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (Music.musicState == Music.MusicState.notPlaying)
            {
                Music.musicState = Music.MusicState.nowPlaying;
                Music.Level1();
            }
            if (MediaPlayer.State == MediaState.Stopped)
                Music.musicState = Music.MusicState.notPlaying;

            player1.Update(gameTime);
            camera.Update(player1.Position, map1.Width, map1.Height, 1);
            parallaxeBackground.Update(player1.Position);

            foreach (CollisionTiles tile in map1.CollisionTiles)
            {
                player1.Collision(tile.Rectangle, map1.Width, map1.Height);
            }
            foreach (CollectionTiles tile in map1.CollectionTiles.ToList())
            {
                if (player1.playerRectangle.Intersects(tile.Rectangle))
                {
                    map1.CollectionTiles.Remove(tile);
                    SoundFx.StrawEffect();

                    //Particles
                    List<Texture2D> texture = new List<Texture2D>();
                    texture.Add(Content.Load<Texture2D>("Particles/StarParticle"));
                    particleEngine = new ParticleEngine(texture, new Vector2(0));
                    particle = true; timer = 0f;
                    particleEngine.EmitterLocation = new Vector2(player1.Position.X + 32, player1.Position.Y + 128);
                    //Particles

                    strawCount.Count();
                }
            }
            if (strawCount.strawScale)
            { strawCount.Update(gameTime, Content); }

            if (particle == true)
            {
                Random random = new Random();
                if (timer < 750)
                    particleEngine.Update(5, 1f, 1f, 4, 4, (float)random.NextDouble(), (float)random.NextDouble(), (float)random.NextDouble(), true, 1, 2, new Vector2(0));

                else particle = false;
            }


            if (LevelEndState.endLevel == LevelEndState.EndState.Level1End)
            {
                WorldMap.LevelDone1 = true;
                WorldMap.EnterInstruction = false;
                WorldMap.EnterLevel = true;
                WorldMap.BackFromLevel1 = true;
                levelEnd.Update(gameTime, ks, strawCount);
                
            }

                 
        
        }



        public void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Begin();
            parallaxeBackground.Draw(spriteBatch);
            spriteBatch.End();

            //Dessin nécessitant le suivi de la caméra dans level 1
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, camera.Transform);
            map1.Draw(spriteBatch);
            if (particle == true)
            { particleEngine.Draw(spriteBatch); }
            player1.Draw(spriteBatch);
            spriteBatch.End();

            spriteBatch.Begin();
            strawCount.Draw(spriteBatch);
            if (LevelEndState.endLevel == LevelEndState.EndState.Level1End)
            {
                levelEnd.Draw(spriteBatch);
                
            }
            spriteBatch.End();

            

                
                

            

        }



    }
}
