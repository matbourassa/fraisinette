﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace Fraisinette
{
    class Player1
    {
        float timer = 0f;
        float interval = 60f;

        int currentframeRight = 8;
        int currentframeLeft = 0;
        int jumpframeRight = 17;
        int jumpframeLeft = 16;     
        int spriteHeight = 128;
        int spriteWidth = 64;
                        
        Texture2D texture;
        public Vector2 velocity;
        public Rectangle playerRectangle;

        public Rectangle frameRectangleRight;
        public Rectangle frameRectangleLeft;
        public Rectangle frameRectangleJumpRight;
        public Rectangle frameRectangleJumpLeft;

        public enum PersonnageState { standingRight, standingLeft, walkingRight, walkingLeft, jumpingRight, jumpingLeft, movingRight, movingLeft };
        public PersonnageState personnageState = new PersonnageState();

        bool hasJump = false;
        bool right = true;
        

        private Vector2 position;
        public Vector2 Position
        { get { return position; } }
        
        


        public void Initialize()
        {
            personnageState = PersonnageState.standingRight;
            velocity = new Vector2(0, 0);
            position = new Vector2(200, 832);   
        }


        public void Load(ContentManager Content)
        {
            texture = Content.Load<Texture2D>("FraisinetteSheet"); 
        }

        
        public void Update(GameTime gameTime)
        {

            
            if (currentframeRight > 17) currentframeRight = 17; //Pour éviter de tomber en bas des images permises
            if (currentframeLeft > 16) currentframeLeft = 16;   //Pour une raison que j'ignore, la machine s'excite et dépasse 17 des fois

            position += velocity;
            playerRectangle = new Rectangle((int)position.X, (int)position.Y, spriteWidth, spriteHeight);
            timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            frameRectangleRight = new Rectangle(currentframeRight * spriteWidth, 0, spriteWidth, spriteHeight);
            frameRectangleLeft = new Rectangle(currentframeLeft * spriteWidth, 0, spriteWidth, spriteHeight);
            frameRectangleJumpRight = new Rectangle(jumpframeRight * spriteWidth, 0, spriteWidth, spriteHeight);
            frameRectangleJumpLeft = new Rectangle(jumpframeLeft * spriteWidth, 0, spriteWidth, spriteHeight);

            StateInput();
            Animation(gameTime);

            if (velocity.Y < 10)
                velocity.Y += 0.4f;
        }


        private void StateInput()
        {
            KeyboardState keystate = Keyboard.GetState();
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);
            
            
            
            
            if ((keystate.IsKeyDown(Keys.Right)) || (gamePadState.DPad.Right == ButtonState.Pressed))
                { personnageState = PersonnageState.walkingRight; right = true; }

           if ((keystate.IsKeyDown(Keys.Left)) || (gamePadState.DPad.Left == ButtonState.Pressed))
                { personnageState = PersonnageState.walkingLeft; right = false; }
            
            
            if (hasJump == false)
            {
                if (keystate.IsKeyDown(Keys.Space) || (gamePadState.Buttons.A == ButtonState.Pressed) && (personnageState == PersonnageState.standingRight))
                { personnageState = PersonnageState.jumpingRight; }

                if (keystate.IsKeyDown(Keys.Space) || (gamePadState.Buttons.A == ButtonState.Pressed) && (personnageState == PersonnageState.standingLeft))
                { personnageState = PersonnageState.jumpingLeft; }

                if (keystate.IsKeyDown(Keys.Space) || (gamePadState.Buttons.A == ButtonState.Pressed) && (personnageState == PersonnageState.walkingRight))
                { personnageState = PersonnageState.jumpingRight; }

                if (keystate.IsKeyDown(Keys.Space) || (gamePadState.Buttons.A == ButtonState.Pressed) && (personnageState == PersonnageState.walkingLeft))
                { personnageState = PersonnageState.jumpingLeft; }

            }

        }

        private void Animation(GameTime gameTime)
        {
           
            if (personnageState == PersonnageState.walkingRight)
            {
                if (currentframeRight > 12) currentframeRight = 8;  //encore une fois, malgré que je lui demande quelque chose à 12, il dépasse parfois...
                velocity.X = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 2.5f;

                if (hasJump == false)
                {
                    if (timer > interval)
                    {
                        currentframeRight++;
                        timer = 0;
                    }
                    if (currentframeRight == 12)
                    {
                        currentframeRight = 8;
                        personnageState = PersonnageState.standingRight;
                    }
                }
                else currentframeRight = 17;
                
            }

            else if (personnageState == PersonnageState.walkingLeft)
            {
                if (currentframeLeft > 4) currentframeLeft = 0; //encore une fois, malgré que je lui demande quelque chose à 4, il dépasse parfois...
                velocity.X = -(float)gameTime.ElapsedGameTime.TotalMilliseconds / 2.5f;

                if (hasJump == false)
                {
                    if (timer > interval)
                    {
                        currentframeLeft++;
                        timer = 0;
                    }

                    if (currentframeLeft == 4)
                    {
                        currentframeLeft = 0;
                        personnageState = PersonnageState.standingLeft;
                    }
                }
                else currentframeLeft = 16;
            }
            
            else if ((hasJump == false) && (personnageState == PersonnageState.jumpingRight))
                            
            {
                
                SoundFx.PlayJumpEffect();
                position.Y -= 5f;
                velocity.Y = -12f;
                if (right == true)
                    currentframeRight = 17;
                else currentframeLeft = 16;
                hasJump = true;
            }

            else if ((hasJump == false) && (personnageState == PersonnageState.jumpingLeft))
            {
                
                SoundFx.PlayJumpEffect();
                position.Y -= 5f;
                velocity.Y = -12f;
                if (right == true)
                    currentframeRight = 17;
                else currentframeLeft = 16;
                hasJump = true;
            }
            
            else
            {
                velocity.X = 0f;

                if (right == true) personnageState = PersonnageState.standingRight;
                else personnageState = PersonnageState.standingLeft;
            }

        }

        
        //DÉTECTION DES COLLISIONS AVEC LES TILES

        public void Collision(Rectangle newRectangle, int xOffSet, int yOffSet)
        {
            if (playerRectangle.TouchTopOf(newRectangle))  
            {

                SoundFx.StopJumpEffect();
                playerRectangle.Y = newRectangle.Y - playerRectangle.Height;
                velocity.Y = 0f;

                if ((hasJump == true) && (right == true))
                { currentframeRight = 8; personnageState = PersonnageState.standingRight; }
                if ((hasJump == true) && (right == false))
                { currentframeLeft = 0; personnageState = PersonnageState.standingLeft; }
                hasJump = false;
                
            }

            if (playerRectangle.TouchBottomOf(newRectangle))
            {
                velocity.Y = 1f;
            }
            
            if (playerRectangle.TouchLeftOf(newRectangle))
            {
                position.X = newRectangle.X - playerRectangle.Width - 1;
                
            }

            if (playerRectangle.TouchRightOf(newRectangle))
            {
                position.X = newRectangle.X + newRectangle.Width + 1;
                
            }

            //Limite du tableau
            if (position.X < 0) position.X = 0;
            if (position.X > xOffSet - playerRectangle.Width) position.X = xOffSet - playerRectangle.Width;
            if (position.Y < 0) velocity.Y = 1f;
            if ((position.Y > yOffSet - (playerRectangle.Height * 2.5f)) && GameState.playerState == GameState.PlayerState.alive)
            {
                GameState.playerState = GameState.PlayerState.falling;
                SoundFx.PlayFall1();
                
            }
            if (position.Y > yOffSet + playerRectangle.Height)
            {
                
                GameState.playerState = GameState.PlayerState.dead;
            }
               
               
            
            
       }


        public void Draw(SpriteBatch spriteBatch)
        {
            
            //Standing
            if (personnageState == PersonnageState.standingRight)
                spriteBatch.Draw(texture, playerRectangle, frameRectangleRight, Color.White);

            if (personnageState == PersonnageState.standingLeft)
                spriteBatch.Draw(texture, playerRectangle, frameRectangleLeft, Color.White);
            
            //Walking
            if (personnageState == PersonnageState.walkingRight)
                spriteBatch.Draw(texture, playerRectangle, frameRectangleRight, Color.White);

            if (personnageState == PersonnageState.walkingLeft)
                spriteBatch.Draw(texture, playerRectangle, frameRectangleLeft, Color.White);

            //Jumping
            if (personnageState == PersonnageState.jumpingRight)
                spriteBatch.Draw(texture, playerRectangle, frameRectangleJumpRight, Color.White);

            if (personnageState == PersonnageState.jumpingLeft)
                spriteBatch.Draw(texture, playerRectangle, frameRectangleJumpLeft, Color.White);

            
        }

    }
}
