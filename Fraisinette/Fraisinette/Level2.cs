﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fraisinette
{
    class Level2
    {

        public Map map2 = new Map();
        public Map map2back = new Map();
        Player1 player1 = new Player1();
        Camera camera;
        Camera cameraback;
        CloudScroller cloudScroller = new CloudScroller();
        StrawCount strawCount = new StrawCount();
        ParticleEngine LeafsParticleEngine;
        ParticleEngine StrawParticleEngine;
        LevelEnd levelEnd = new LevelEnd();
        Texture2D SkyTexture;
        Vector2 SkyPosition;

        bool leafParticle = false;
        bool strawParticle = false;
        float timer = 0f;
        Vector2 Gravity;
        
        
        public void Initialize()
        {
            player1.Initialize();
            SkyPosition = new Vector2(0);
            Gravity = new Vector2(0);
        }

        
        public void Load(ContentManager Content, GraphicsDevice graphicsDevice)
        {
            camera = new Camera(graphicsDevice.Viewport);
            cameraback = new Camera(graphicsDevice.Viewport);
            Tiles.content = Content;
            map2.LoadLevel2();
            map2back.LoadLevel2Back();
            player1.Load(Content);
            strawCount.Load(Content, 1);
            levelEnd.LoadContent(Content);
            cloudScroller.LoadContent(Content);
            SkyTexture = Content.Load<Texture2D>("Level2/SkyBackLevel2");
        }

        public void Update(ContentManager Content, GameTime gameTime, KeyboardState ks)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (Music.musicState == Music.MusicState.notPlaying)
            {
                Music.musicState = Music.MusicState.nowPlaying;
                Music.Level2();
            }
            if (MediaPlayer.State == MediaState.Stopped)
                Music.musicState = Music.MusicState.notPlaying;

                        
            player1.Update(gameTime);
            camera.Update(player1.Position, map2.Width, map2.Height, 1);
            cameraback.Update(player1.Position, map2back.Width, map2back.Height, 0.5f);
            cloudScroller.Update();
            LeafTile(Content);
            CollisionTile(Content);
            CollectionTile(Content, gameTime);
            
            if (LevelEndState.endLevel == LevelEndState.EndState.Level2End)
            {
                WorldMap.LevelDone2 = true;
                WorldMap.EnterInstruction = false;
                WorldMap.EnterLevel = true;
                WorldMap.BackFromLevel2 = true;
                levelEnd.Update(gameTime, ks, strawCount);
                
            }

                       
        
        }

        
        public void Draw(SpriteBatch spriteBatch)
        {

            //Skybox
            spriteBatch.Begin();
            spriteBatch.Draw(SkyTexture, SkyPosition, Color.White);
            spriteBatch.End();

            //Camera BackGround
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cameraback.Transform);
            map2back.Draw(spriteBatch);
            cloudScroller.Draw(spriteBatch);
            spriteBatch.End();

            //Camera Principale
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, camera.Transform);
            map2.Draw(spriteBatch);
            if (leafParticle == true)
            { LeafsParticleEngine.Draw(spriteBatch); }
            if (strawParticle == true)
            { StrawParticleEngine.Draw(spriteBatch); }
            player1.Draw(spriteBatch);
            spriteBatch.End();
            
            //UI static
            spriteBatch.Begin();
            strawCount.Draw(spriteBatch);
            if (LevelEndState.endLevel == LevelEndState.EndState.Level2End)
            {
              levelEnd.Draw(spriteBatch);
            }
            spriteBatch.End();
        
        }




        void LeafTile(ContentManager Content)
        {
            foreach (EvenementTiles tile in map2.EvenementTiles)  //Particules de feuilles mortes
            {
                if ((player1.personnageState == Player1.PersonnageState.jumpingLeft) || (player1.personnageState == Player1.PersonnageState.jumpingRight) //Pour prévenir les feuilles de poper tout le temps que on ne bouge pas
                    || (player1.personnageState == Player1.PersonnageState.walkingLeft) || (player1.personnageState == Player1.PersonnageState.walkingRight))
                {

                    if (player1.playerRectangle.Intersects(tile.Rectangle))
                    {
                        if (leafParticle == false) //Pour éviter que les feuilles recommence le cycle sans arrêt et reste ''collées'' aux pieds de fraisinette
                        {
                            SoundFx.LeafEffect2();

                            List<Texture2D> leafTexture = new List<Texture2D>();
                            leafTexture.Add(Content.Load<Texture2D>("Particles/FeuilleMorte"));
                            LeafsParticleEngine = new ParticleEngine(leafTexture, new Vector2(player1.Position.X + 0, player1.Position.Y + 128));
                            leafParticle = true; timer = 0f;
                        }
                    }
                }
            }

            if (leafParticle == true)
            {
                Random random = new Random();
                Gravity = new Vector2(0, +0.05f);
                if (timer < 1000)
                    LeafsParticleEngine.Update(1, 1f, 3f, 4, 1, 180, (float)random.NextDouble(), 0, false, 0.75f, 40, Gravity); //voir ParticleEngine pour explication des paramètres

                else { leafParticle = false; Gravity = new Vector2(0); };
            }
        }


        void CollisionTile(ContentManager Content)
        {
            foreach (CollisionTiles tile in map2.CollisionTiles)
            {
                player1.Collision(tile.Rectangle, map2.Width, map2.Height);
            }


            if (GameState.playerState == GameState.PlayerState.dead)
            {
                GameState.playerState = GameState.PlayerState.alive;
                SoundFx.PlayFall2();
                player1.Initialize();
            }
        }

        void CollectionTile(ContentManager Content, GameTime gameTime)
        {
            foreach (CollectionTiles tile in map2.CollectionTiles.ToList())
            {
                if (player1.playerRectangle.Intersects(tile.Rectangle))
                {

                    map2.CollectionTiles.Remove(tile);
                    SoundFx.StrawEffect();

                    //Particles
                    List<Texture2D> strawTexture = new List<Texture2D>();
                    strawTexture.Add(Content.Load<Texture2D>("Particles/StarParticle"));
                    StrawParticleEngine = new ParticleEngine(strawTexture, new Vector2(player1.Position.X + 32, player1.Position.Y + 64));
                    strawParticle = true; timer = 0f;


                    //Fraises ramassées
                    strawCount.Count();
                }
            }

            if (strawParticle == true)
            {
                Random random = new Random();
                if (timer < 500)
                    StrawParticleEngine.Update(3, 2f, 1f, 2, 2, 0, (float)random.NextDouble() +0.25f, 0, false, 0.5f, 40, new Vector2(0));

                else strawParticle = false;
            }

            if (strawCount.strawScale)
            { strawCount.Update(gameTime, Content); }
        }
    }
}
