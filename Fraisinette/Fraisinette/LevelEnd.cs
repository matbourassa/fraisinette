﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fraisinette
{
    class LevelEnd
    {

        Texture2D SplashTexture;
        Vector2 SplashPosition = new Vector2(0);
        bool soundfx = true;
        float timer = 0;
        



        public void LoadContent(ContentManager content)
        {
            SplashTexture = content.Load<Texture2D>("Level1/LevelEnd");
        
        }



        public void Update(GameTime gameTime, KeyboardState ks, StrawCount strawCount)
        {

            
            timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            MediaPlayer.Stop();
            Music.musicState = Music.MusicState.nowPlaying;

            if (soundfx)
            { soundfx = false; SoundFx.PlayLevelEndEffect(); }


            if (timer > 4400)
            {
                
                Music.musicState = Music.MusicState.notPlaying;
                strawCount.Strawberry = 0;
                timer = 0;
                soundfx = true;
                LevelEndState.endLevel = LevelEndState.EndState.NoLevelEnd;
                GameState.state = GameState.State.worldMap;
                
                
            }

            if (ks.IsKeyDown(Keys.Enter))
            {
                SoundFx.StopLevelEndEffect();
                Music.musicState = Music.MusicState.notPlaying;
                strawCount.Strawberry = 0;
                timer = 0;
                soundfx = true;
                LevelEndState.endLevel = LevelEndState.EndState.NoLevelEnd;
                GameState.state = GameState.State.worldMap;
                
            }
             
        }


        public void Draw(SpriteBatch spriteBatch)
        {

            
            spriteBatch.Draw(SplashTexture, SplashPosition, Color.White);
            
        }



    }
}
