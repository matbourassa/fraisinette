using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fraisinette
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {
                
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
                
                
        SplashScreen splashScreen;
        Level1 level1;
        Level2 level2;
        Level3 level3;
        WorldMap worldMap;
        


        //******************************** C O N S T R U C T E U R*************************************************************
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            splashScreen = new SplashScreen();
            level1 = new Level1();
            level2 = new Level2();
            level3 = new Level3();
            worldMap = new WorldMap(level1, level2, level3);
        }

        //********************************* I N I T I A L I Z E *******************************************************************

        protected override void Initialize()
        {
            //Full screen + grandeur de l'�cran
            graphics.PreferredBackBufferWidth = (1680);
            graphics.PreferredBackBufferHeight = (1050);
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();
            spriteBatch = new SpriteBatch(GraphicsDevice);

            level1.Initialize();
            level2.Initialize();
            level3.Initialize();
            GameState.Initialize();
            
            
            base.Initialize();
        }

        //************************************** L O A D  C O N T E N T ******************************************************

        protected override void LoadContent()
        {

                        
            splashScreen.Load(Content);
            worldMap.Load(Content);
            level1.Load(Content, GraphicsDevice);
            level2.Load(Content, GraphicsDevice);
            level3.Load(Content, GraphicsDevice);
            
            SoundFx.Load(Content);
            Music.Load(Content);
                             
        }

        //************************************ U N L O A D   C O N T E N T ****************************************
        protected override void UnloadContent()
        { }
        //***************************************** U P D A T E ******************************************************

        protected override void Update(GameTime gameTime)
        {

            //�l�ments communs**********************
            KeyboardState ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Escape))
                this.Exit();
                        
            
            //SplashScreen******************************************
            if (GameState.state == GameState.State.splashScreen)
            {
                splashScreen.Update(gameTime, ks);
            }

            
            //WorldMap********************************************
            if (GameState.state == GameState.State.worldMap)
            {
                worldMap.Update(ks, Content, GraphicsDevice);
            }
            

            //Level1**********************************************
            if (GameState.state == GameState.State.level1)
            { 
                level1.Update(Content, gameTime, ks);
            }

            //Level2**********************************************
            if (GameState.state == GameState.State.level2)
            {
                level2.Update(Content, gameTime, ks);
            }

            //Level3**********************************************
            if (GameState.state == GameState.State.level3)
            {
                level3.Update(Content, gameTime, ks);
            }
            

            base.Update(gameTime);
        }
        

        //************************************ D R A W ***************************************************
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);



            //Splashscreen***********************
            if (GameState.state == GameState.State.splashScreen)
            {
                spriteBatch.Begin();
                splashScreen.Draw(spriteBatch);
                spriteBatch.End();
            
            }
            
            //WorldMap*****************************
            if (GameState.state == GameState.State.worldMap)
            {
                spriteBatch.Begin();
                worldMap.Draw(spriteBatch);
                spriteBatch.End();
            }

            //Level1 *******************
            if (GameState.state == GameState.State.level1)
            {
                level1.Draw(spriteBatch);
            }

            //Level2 *******************
            if (GameState.state == GameState.State.level2)
            {
                level2.Draw(spriteBatch);
            }

            //Level3 *******************
            if (GameState.state == GameState.State.level3)
            {
                level3.Draw(spriteBatch);
            }        


            base.Draw(gameTime);
        }

  }


}


        
          





    



