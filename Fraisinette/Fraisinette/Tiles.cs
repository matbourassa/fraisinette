﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Fraisinette
{
    class Tiles
    {
        public Texture2D texture;
        
        private Rectangle rectangle;
        public Rectangle Rectangle
        {
            get { return rectangle; }
            set { rectangle = value; }
        }

        public static ContentManager content;
        public static ContentManager ContentManager
        {
            get { return content; }
            set { content = value; }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
         spriteBatch.Draw(texture, rectangle, Color.White);
        }
    }


    class CollisionTiles : Tiles
    {
        public CollisionTiles(int i, Rectangle newRectangle)
        {
            texture = content.Load<Texture2D>("Tiles/Tile" + i);
            this.Rectangle = newRectangle;
        }
    
    }
    
    class DecorationTiles : Tiles
    {

        public DecorationTiles(int i, Rectangle newRectangle)
        {
           texture = content.Load<Texture2D>("Tiles/Tile" + i);
           this.Rectangle = newRectangle;
        }
        
    }

    class CollectionTiles : Tiles
    {

        public CollectionTiles(int i, Rectangle newRectangle)
        {
            texture = content.Load<Texture2D>("Tiles/Tile" + i);
            this.Rectangle = newRectangle;
        }
    }

    class EvenementTiles : Tiles
    {

        public EvenementTiles(int i, Rectangle newRectangle)
        {
            texture = content.Load<Texture2D>("Tiles/Tile" + i);
            this.Rectangle = newRectangle;
        }
    }
}