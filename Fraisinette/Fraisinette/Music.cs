﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework;

namespace Fraisinette
{
    static class Music
    {

        

        public static Song level1;
        public static Song level2;
        public static Song level3;
        public static Song theme;
        public static Song themeInstrument;
        
        
        public enum MusicState { nowPlaying, notPlaying }
        public static MusicState musicState = new MusicState();
                
        public static float MatVolume = 0.25f;

        public static void Load(ContentManager content)
        {
            level1 = content.Load<Song>("Music/Politesse");
            level2 = content.Load<Song>("Music/BallonVole");
            level3 = content.Load<Song>("Music/Demoiselle");
            theme = content.Load<Song>("Music/Theme");
            themeInstrument = content.Load<Song>("Music/ThemeInstrument");
            musicState = MusicState.notPlaying;
            MediaPlayer.Volume = MatVolume;
            
        }

        //************************************

        
        public static void Level1()
        {
            MediaPlayer.Play(level1);
        }
        
        public static void Level2()
        {
            MediaPlayer.Play(level2);
        }

        public static void Level3()
        {
            MediaPlayer.Play(level3);
        }

        public static void Theme()
        {
            MediaPlayer.Play(theme);
        }

        public static void ThemeInstrument()
        {
            MediaPlayer.Play(themeInstrument);
        }

        


    }
}
