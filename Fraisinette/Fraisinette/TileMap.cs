﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fraisinette
{

    class MapRow
    {
        public List<MapCell> Columns = new List<MapCell>();
    }



    class TileMap
    {

        public List<MapRow> Rows = new List<MapRow>();
        public int MapWidth = 80;
        public int MapHeight = 23;



        public TileMap()
        {
            for (int y = 0; y < MapHeight; y++)
            {
                MapRow thisRow = new MapRow();
                for (int x = 0; x < MapWidth; x++)
                {
                    thisRow.Columns.Add(new MapCell(8));
                }
                Rows.Add(thisRow);
            }

            
            Rows[15].Columns[7].TileID = 4;
            Rows[15].Columns[8].TileID = 4;
            Rows[15].Columns[9].TileID = 4;
            Rows[15].Columns[10].TileID = 4;
            Rows[15].Columns[11].TileID = 4;

            Rows[15].Columns[17].TileID = 4;
            Rows[15].Columns[18].TileID = 4;
            Rows[15].Columns[19].TileID = 4;
            Rows[15].Columns[20].TileID = 4;
            Rows[15].Columns[21].TileID = 4;

            Rows[17].Columns[25].TileID = 4;
            Rows[17].Columns[26].TileID = 4;
            Rows[17].Columns[27].TileID = 4;
            Rows[17].Columns[28].TileID = 4;
            Rows[17].Columns[29].TileID = 4;
            Rows[17].Columns[30].TileID = 4;

            Rows[18].Columns[32].TileID = 4;
            Rows[18].Columns[33].TileID = 4;
            Rows[18].Columns[34].TileID = 4;
            Rows[18].Columns[35].TileID = 4;
            Rows[18].Columns[36].TileID = 4;
            Rows[18].Columns[37].TileID = 4;

            Rows[14].Columns[52].TileID = 4;
            Rows[14].Columns[53].TileID = 4;
            Rows[14].Columns[54].TileID = 4;
            Rows[14].Columns[55].TileID = 4;
            Rows[14].Columns[56].TileID = 4;
            Rows[14].Columns[57].TileID = 4;

            Rows[15].Columns[65].TileID = 4;
            Rows[15].Columns[66].TileID = 4;
            Rows[15].Columns[67].TileID = 4;
            Rows[15].Columns[68].TileID = 4;
            Rows[15].Columns[69].TileID = 4;
            Rows[15].Columns[70].TileID = 4;




            /*
                
            Rows[15].Columns[7].AddBaseTile(60);
            Rows[15].Columns[8].AddBaseTile(60);
            Rows[15].Columns[9].AddBaseTile(60);

            Rows[3].Columns[6].AddBaseTile(25);
            Rows[5].Columns[6].AddBaseTile(24);

            Rows[3].Columns[7].AddBaseTile(31);
            Rows[4].Columns[7].AddBaseTile(26);
            Rows[5].Columns[7].AddBaseTile(29);

            Rows[4].Columns[6].AddBaseTile(104);
            */
            
        }



    }




}
