﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Fraisinette
{
    class Background : Sprite
    {



        //Constructeur
        public Background()
        {
            spritePosition = new Vector2(0);
        }






        public override void LoadContent(ContentManager Content)
        {

            spriteTexture = Content.Load<Texture2D>("Backgroundtest1");
            
        }



        public void UpdateRight(GameTime gameTime, GraphicsDevice graphicsDevice)

        {

            spritePosition += spriteDirection * spriteSpeed * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            spriteDirection = -Vector2.UnitX;
            spriteSpeed = new Vector2(0.3f);


        }

        public void UpdateLeft(GameTime gameTime, GraphicsDevice graphicsDevice)
        {

            spritePosition += spriteDirection * spriteSpeed * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            spriteDirection = Vector2.UnitX;
            spriteSpeed = new Vector2(0.3f);


        }




    }
}
