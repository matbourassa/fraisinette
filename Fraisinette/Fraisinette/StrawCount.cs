﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Fraisinette
{
    class StrawCount
    {
               

        public Vector2 positionStraw = new Vector2(715, 40);
        Vector2 positionText = new Vector2(780, 30);
        Texture2D texture;
        SpriteFont font;
        float timer;
        float interval = 10f;
        public bool strawScale;
        float scale = 1f;
        public int Strawberry = 0;
        
        
        
        public void Load(ContentManager content, int choice)
        {
            if (choice == 1)
                texture = content.Load<Texture2D>("Tiles/Tile61");
            if (choice == 2)
                texture = content.Load<Texture2D>("Level3/FraisiMini");


            font = content.Load<SpriteFont>("Fonts/SpriteFont1");
            
        }

        public void Count()
        {
            Strawberry++;
            strawScale = true;
        }     
            

        public void Update(GameTime gameTime, ContentManager content)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (timer > interval)
            {
                timer = 0f;
                scale += 0.1f;


                if (scale > 1.7f)
                {
                    scale = 1f;
                    strawScale = false;
                }
            }

            if ((GameState.state == GameState.State.level1) && (Strawberry >= 100))
            { LevelEndState.endLevel = LevelEndState.EndState.Level1End;}

            if ((GameState.state == GameState.State.level2) && (Strawberry >= 75))
            { LevelEndState.endLevel = LevelEndState.EndState.Level2End;}

            if ((GameState.state == GameState.State.level3) && (Strawberry >= 12))
            { LevelEndState.endLevel = LevelEndState.EndState.Level3End; }


        
        }

        public void Draw(SpriteBatch spritebatch)
        {

            spritebatch.Draw(texture, positionStraw, null, Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, 0f);
            spritebatch.DrawString(font, " = "+Strawberry, positionText, Color.IndianRed);
            
        }

    }

    

    
   






}
