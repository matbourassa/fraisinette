﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Fraisinette
{
    class Level3
    {

        public Map map3;
        Player1  player1;
        Camera camera;
        Camera camera2;
        Camera camera3;
        Camera camera4;
        ParticleEngine StrawParticleEngine;
        ParticleEngine SnowFlakeParticle;
        StrawCount FraisiCount;
        Enemie badGuyTest1;
        Enemie badGuyTest2;
        Enemie badGuyTest3;
        Enemie badGuyTest4;
        Enemie badGuyTest5;
        Enemie badGuyTest6;
        Enemie badGuyTest7;
        Enemie badGuyTest8;
        Enemie badGuyTest9;
        Enemie badGuyTest10;
        Enemie badGuyTest11;
        Enemie badGuyTest12;
        LevelEnd levelEnd = new LevelEnd();
        
        float timer = 0f;
        bool strawParticle = false;

        Texture2D frontgroundTexture;
        Vector2 frontgroundPosition = new Vector2(200, 0);
        Texture2D frontgroundTexture2;
        Vector2 frontgroundPosition2 = new Vector2(3800, 0);
        Texture2D background1Texture;
        Vector2 background1Position = new Vector2(0, 0);
        Texture2D background2Texture;
        Vector2 background2Position = new Vector2(4000,0);
        Texture2D fargroundTexture;
        Vector2 fargroundPosition = new Vector2(0, 0);
        Texture2D SkyBoxTexture;
        Vector2 SkyBoxPosition = new Vector2(0);

        List<Enemie> enemies = new List<Enemie>();


        public Level3()
        {
            map3 = new Map();
            player1 = new Player1();
            FraisiCount = new StrawCount();
            
            
        }


        public void Initialize()
        {
            player1.Initialize();
            enemies.Add(badGuyTest1 = new Enemie(player1, new Vector2(800, 760), 1));
            enemies.Add(badGuyTest2 = new Enemie(player1, new Vector2(1200, 1064), 2));
            enemies.Add(badGuyTest3 = new Enemie(player1, new Vector2(1600, 832), 1));
            enemies.Add(badGuyTest4 = new Enemie(player1, new Vector2(1660, 440), 3));
            enemies.Add(badGuyTest5 = new Enemie(player1, new Vector2(3712, 648), 2));
            enemies.Add(badGuyTest6 = new Enemie(player1, new Vector2(4612, 1048), 1));
            enemies.Add(badGuyTest7 = new Enemie(player1, new Vector2(5612, 848), 3));
            enemies.Add(badGuyTest8 = new Enemie(player1, new Vector2(5612, 1048), 2));
            enemies.Add(badGuyTest9 = new Enemie(player1, new Vector2(6812, 548), 3));
            enemies.Add(badGuyTest10 = new Enemie(player1, new Vector2(7612, 348), 1));
            enemies.Add(badGuyTest11 = new Enemie(player1, new Vector2(8012, 848), 3));
            enemies.Add(badGuyTest12 = new Enemie(player1, new Vector2(9012, 248), 2));
            foreach (Enemie enemie in enemies)
            { enemie.Initialize(); }


        }

        
        public void Load(ContentManager Content, GraphicsDevice graphicsDevice)
        {
            camera = new Camera(graphicsDevice.Viewport);
            camera2 = new Camera(graphicsDevice.Viewport);
            camera3 = new Camera(graphicsDevice.Viewport);
            camera4 = new Camera(graphicsDevice.Viewport);
            
            List<Texture2D> snowFlakeList = new List<Texture2D>(); 
            snowFlakeList.Add(Content.Load<Texture2D>("Level3/SnowFlake"));
            SnowFlakeParticle = new ParticleEngine(snowFlakeList, new Vector2(0));
            
            Tiles.content = Content;
            map3.LoadLevel3();
            player1.Load(Content);
            FraisiCount.Load(Content, 2);
            levelEnd.LoadContent(Content);
            frontgroundTexture = Content.Load<Texture2D>("Level3/SnowFrontground");
            frontgroundTexture2 = Content.Load<Texture2D>("Level3/SnowFrontground");
            background1Texture = Content.Load<Texture2D>("Level3/SnowBackground");
            background2Texture = Content.Load<Texture2D>("Level3/SnowBackground");
            fargroundTexture = Content.Load<Texture2D>("Level3/SnowFarground");
            SkyBoxTexture = Content.Load<Texture2D>("LEvel2/SkyBackLevel2");
            foreach (Enemie enemie in enemies)
            { enemie.Load(Content); }
                  
        }

        
        public void Update(ContentManager content, GameTime gameTime, KeyboardState ks)
        {

            if (Music.musicState == Music.MusicState.notPlaying)
            {
                Music.musicState = Music.MusicState.nowPlaying;
                Music.Level3();
            }
            if (MediaPlayer.State == MediaState.Stopped)
                Music.musicState = Music.MusicState.notPlaying;
            
            
            camera.Update(player1.Position, map3.Width, map3.Height, 1);
            camera2.Update(player1.Position, map3.Width, map3.Height, 0.5f);
            camera3.Update(player1.Position, map3.Width, map3.Height, 0.25f);
            camera4.Update(player1.Position, map3.Width, map3.Height, 0.7f);
            player1.Update(gameTime);
            
            foreach (Enemie enemie in enemies)
            { enemie.Update(gameTime);}


            foreach (Enemie enemie in enemies.ToList())
            {
                if (player1.playerRectangle.Intersects(enemie.rectangle))
                {
                    FraisiCount.Count();
                    SoundFx.PlayFraisiMini2();
                    enemies.Remove(enemie);
                }
            }
            
                                  
            CollisionTile(content);
            CollectionTile(content, gameTime);
            EvenementTile(content);
            DetectionZone();
            SnowFlake();

            if (LevelEndState.endLevel == LevelEndState.EndState.Level3End)
            {
                WorldMap.LevelDone3 = true;
                WorldMap.EnterInstruction = false;
                WorldMap.EnterLevel = true;
                WorldMap.BackFromLevel3 = true;
                levelEnd.Update(gameTime, ks, FraisiCount);

            }

                  
        
        }


        public void Draw(SpriteBatch spriteBatch)
        {


            spriteBatch.Begin();
            spriteBatch.Draw(SkyBoxTexture, SkyBoxPosition, Color.White);
            spriteBatch.End();
            
            
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, camera3.Transform);
            spriteBatch.Draw(fargroundTexture, fargroundPosition, Color.White);
            spriteBatch.End();


            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, camera2.Transform);
            spriteBatch.Draw(background1Texture, background1Position, Color.White);
            spriteBatch.Draw(background2Texture, background2Position, Color.White);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, camera4.Transform);
            spriteBatch.Draw(frontgroundTexture, frontgroundPosition, Color.White);
            spriteBatch.Draw(frontgroundTexture2, frontgroundPosition2, Color.White);
            spriteBatch.End();

                        
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, camera.Transform);
            map3.Draw(spriteBatch);
            player1.Draw(spriteBatch);
            foreach (Enemie enemie in enemies)
            { enemie.Draw(spriteBatch); }
            SnowFlakeParticle.Draw(spriteBatch);
            spriteBatch.End();

            //UI statique
            spriteBatch.Begin();
            FraisiCount.Draw(spriteBatch);
            if (LevelEndState.endLevel == LevelEndState.EndState.Level3End)
            {
                levelEnd.Draw(spriteBatch);
            }
            spriteBatch.End();

        }

                     


        void CollisionTile(ContentManager Content)
        {
            foreach (CollisionTiles tile in map3.CollisionTiles)
            {
                player1.Collision(tile.Rectangle, map3.Width, map3.Height);
                foreach (Enemie enemie in enemies)
                { enemie.Collision(tile.Rectangle, map3.Width, map3.Height); }
                
            }


            if (GameState.playerState == GameState.PlayerState.dead)
            {
                GameState.playerState = GameState.PlayerState.alive;
                SoundFx.PlayFall2();
                player1.Initialize();
            }
        }


        void CollectionTile(ContentManager Content, GameTime gameTime)
        {
            foreach (CollectionTiles tile in map3.CollectionTiles.ToList())
            {
                if (player1.playerRectangle.Intersects(tile.Rectangle))
                {

                    map3.CollectionTiles.Remove(tile);
                    SoundFx.StrawEffect();

                    //Particles
                    List<Texture2D> strawTexture = new List<Texture2D>();
                    strawTexture.Add(Content.Load<Texture2D>("Particles/StarParticle"));
                    StrawParticleEngine = new ParticleEngine(strawTexture, new Vector2(player1.Position.X + 32, player1.Position.Y + 64));
                    strawParticle = true; timer = 0f;


                    //Fraises ramassées
                    FraisiCount.Count();
                }
            }

            if (strawParticle == true)
            {
                Random random = new Random();
                if (timer < 500)
                    StrawParticleEngine.Update(3, 2f, 1f, 2, 2, 0, (float)random.NextDouble() + 0.25f, 0, false, 0.5f, 40, new Vector2(0));

                else strawParticle = false;
            }

            if (FraisiCount.strawScale)
            { FraisiCount.Update(gameTime, Content); }
        }


        void EvenementTile(ContentManager Content)
        {
            foreach (EvenementTiles tile in map3.EvenementTiles)
            {
                foreach (Enemie enemie in enemies)
                { enemie.Collision(tile.Rectangle, map3.Width, map3.Height); }
            }
        }




        public void DetectionZone()
        {

            

            foreach (Enemie enemie in enemies)
            {
                if ((player1.playerRectangle.Intersects(enemie.detectionZoneRectangle)))
                { enemie.Evading = true; enemie.BackFromEvading = true; }
                else
                { enemie.Evading = false; }

                if ((enemie.Evading == false) && (enemie.BackFromEvading == false)) { enemie.enemieState = Enemie.EnemieState.isPatrolling; enemie.stillAware = false; }
                if ((enemie.Evading == true) && (enemie.BackFromEvading == true)) enemie.enemieState = Enemie.EnemieState.isEvading;
                if ((enemie.Evading == false) && (enemie.BackFromEvading == true)) enemie.enemieState = Enemie.EnemieState.isCalmingDown;
            }

            foreach (Enemie enemie in enemies)
            {
                if ((enemie.stillAware == false) && (player1.playerRectangle.Intersects(enemie.awarnessRectangle)))
                { enemie.enemieState = Enemie.EnemieState.isAware; enemie.stillAware = true; }


            } 

        }


        public void SnowFlake()
        {
            Random random = new Random();
            float Xposition = (float)random.Next(0, 4000);
            SnowFlakeParticle.EmitterLocation = new Vector2(Xposition + 7200, -100);
            SnowFlakeParticle.Update(20, 0.5f, 1.5f, 4, 4, 256, 256, 256, true, 1, 400, new Vector2(-0.03f, 0));   
            
        }

    }
}
