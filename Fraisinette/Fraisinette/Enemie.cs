﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fraisinette
{
    class Enemie
    {
        
        //Le sprite
        Texture2D texture;
        public Vector2 velocity;
        public Rectangle rectangle;
        private Vector2 position;
        const int spriteHeight = 60;
        const int spriteWidth = 38;
        int color;
        
        //Zone de détection
        public Rectangle detectionZoneRectangle;
        const int dectectionZoneWidth = 480;
        const int detectionZoneHeight = 192;
        public Rectangle awarnessRectangle;
        const int awarnessRectangleWidth = 540;
        const int awarnessRectangleHeight = 265;

        //Un timer, c'est toujours pratique
        float timer = 0;
        
        //Variables de détection
        bool JumpRight = false;
        bool JumpLeft = false;
        bool HasJump = false;
        bool PatrollingRight = false;
        bool PatrollingLeft = true;
        bool HasCollide = false;
        public bool BackFromEvading = false;
        public bool Evading = false;
        public enum EnemieState {isIdle, isPatrolling, isAware, isEvading, isCalmingDown };
        public EnemieState enemieState = new EnemieState();
        bool Exclamation = false;
        public bool stillAware = false;

        //Gestion de l'animation
        int currentFrameLeft = 0;
        int currentFrameRight = 4;
        int currentJumpFrameLeft = 8;
        int currentJumpFrameRight = 9;
        Rectangle frameRectangleLeft;
        Rectangle frameRectangleRight;
        Rectangle frameRectangleJumpLeft;
        Rectangle frameRectangleJumpRight;
        public enum AnimationState { standingRight, standingLeft, walkingRight, walkingLeft, runningRight, runningLeft};
        public AnimationState animationState = new AnimationState();
        float interval = 70f;
        float interval2 = 40f;

        //Le petit point d'exclamation
        Texture2D exclamationTexture;
        Vector2 exclamationPosition;
        bool jumpAware = false;
        float timer2;

        
        


        //Classes importées
        Player1 myplayer1;


        //Constructeur
        public Enemie(Player1 player1, Vector2 Position, int Color)
        {
            myplayer1 = player1;
            position = Position;
            color = Color;
        }


        public void Initialize()
        {
        
            velocity = new Vector2(0, 0);
            enemieState = EnemieState.isPatrolling;
            animationState = AnimationState.walkingRight;
        }


        public void Load(ContentManager Content)
        {
                        
            if (color == 1)
            { texture = Content.Load<Texture2D>("Level3/FraisiMiniSheet2"); }
            if (color == 2)
            { texture = Content.Load<Texture2D>("Level3/FraisiMiniSheet2Green"); }
            if (color == 3)
            { texture = Content.Load<Texture2D>("Level3/FraisiMiniSheet2Pink"); }

            
            exclamationTexture = Content.Load<Texture2D>("Level3/exclamation");
        }



        public void Update(GameTime gameTime)
        {
            
            position += velocity;
            rectangle = new Rectangle((int)position.X, (int)position.Y, spriteWidth, spriteHeight); //position du sprite
            detectionZoneRectangle = new Rectangle((int)position.X - 224, (int)position.Y - 132, dectectionZoneWidth, detectionZoneHeight); //Grand rectangle qui part du sol et qui entoure l'enemie
            awarnessRectangle = new Rectangle((int)position.X - 251, (int)position.Y - 132, awarnessRectangleWidth, awarnessRectangleHeight); //Rectangle de awarness plus grand que détection
            exclamationPosition = new Vector2(position.X, position.Y - 80);
            timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            timer2 += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (velocity.Y < 10)
                velocity.Y += 0.4f; //Gravity

            if (Exclamation)
                {
                    if (timer2 >= 700)
                    { Exclamation = false; timer2 = 0; jumpAware = false; }
                }

            //State
            if (enemieState == EnemieState.isIdle)
            { isIdle(); }
            if (enemieState == EnemieState.isEvading)
            { isEvading(); }
            if (enemieState == EnemieState.isCalmingDown)
            { isCalmingDown(gameTime); }
            if (enemieState == EnemieState.isPatrolling)
            { isPatrolling(); }
            if (enemieState == EnemieState.isAware)
            { isAware(gameTime); }
        }

        
        public void Draw(SpriteBatch spriteBatch)
        {

            if (Exclamation == true)
                spriteBatch.Draw(exclamationTexture, exclamationPosition, Color.White);

            if(animationState == AnimationState.walkingRight)
                spriteBatch.Draw(texture, rectangle, frameRectangleRight, Color.White);

            if (animationState == AnimationState.walkingLeft)
                spriteBatch.Draw(texture, rectangle, frameRectangleLeft, Color.White);

            if(animationState == AnimationState.runningRight)
                spriteBatch.Draw(texture, rectangle, frameRectangleRight, Color.White);

            if (animationState == AnimationState.runningLeft)
                spriteBatch.Draw(texture, rectangle, frameRectangleLeft, Color.White);

        }


        //La méthode Collision est ''callé'' par le Level concerné (CollisionTile et EvenementTile)
        public void Collision(Rectangle newRectangle, int xOffSet, int yOffSet)
        {
        if (rectangle.TouchTopOf(newRectangle))  
            {
                rectangle.Y = newRectangle.Y - rectangle.Height;
                velocity.Y = 0f;
                HasJump = false;
            }

            if (rectangle.TouchBottomOf(newRectangle))
            {
                velocity.Y = 1f;
            }
            

            if (rectangle.TouchLeftOf(newRectangle))
            {
                position.X = newRectangle.X - rectangle.Width - 3;
                
                if ((enemieState == EnemieState.isEvading) && (HasJump == false))
                { JumpRight = true; HasJump = true; animationState = AnimationState.runningRight; }

                if ((enemieState == EnemieState.isPatrolling) && (HasCollide == false))
                { PatrollingRight = true; PatrollingLeft = false; HasCollide = true; animationState = AnimationState.walkingLeft; }
                     
            }

            if (rectangle.TouchRightOf(newRectangle))
            {
                position.X = newRectangle.X + newRectangle.Width + 3;
                
                if ((enemieState == EnemieState.isEvading) && (HasJump == false))
                { JumpLeft = true; HasJump = true; animationState = AnimationState.runningLeft; }

                if ((enemieState == EnemieState.isPatrolling) && (HasCollide == false))
                { PatrollingRight = false; PatrollingLeft = true; HasCollide = true; animationState = AnimationState.walkingRight; }
                
            }

            //Collision avec les limites du tableau
            if (position.X < 0)
            {
                position.X = 0; velocity.X = 5; if ((enemieState == EnemieState.isPatrolling) && (HasCollide == false))
                { PatrollingRight = false; PatrollingLeft = true; HasCollide = true; animationState = AnimationState.walkingRight; }
            }
            if (position.X > xOffSet - rectangle.Width)
            {
                position.X = xOffSet - rectangle.Width; velocity.X = -5; if ((enemieState == EnemieState.isPatrolling) && (HasCollide == false))
                { PatrollingRight = true; PatrollingLeft = false; HasCollide = true; animationState = AnimationState.walkingLeft; }
            }

        }
        

        public void isIdle()
        {

            velocity.X = 0;
            
        }

        public void isPatrolling()
        {

            if ((PatrollingRight == true) && (PatrollingLeft == false))
            { velocity.X = -3; HasCollide = false; animationState = AnimationState.walkingLeft; }
            if ((PatrollingLeft == true) && (PatrollingRight == false))
            { velocity.X = 3; HasCollide = false; animationState = AnimationState.walkingRight; }
            Animation();
        }

        public void isEvading()
        {
            if (myplayer1.Position.X <= position.X)
            { velocity.X = 8f; animationState = AnimationState.runningRight; }
            if (myplayer1.Position.X >= position.X)
            { velocity.X = -8f; animationState = AnimationState.runningLeft; }

            if (JumpRight)
            { position.Y -= 8f; velocity.Y = -8f; JumpRight = false; }
            if (JumpLeft)
            { position.Y -= 8f; velocity.Y = -8f; JumpLeft = false; }
            Animation();
        }


        public void isCalmingDown(GameTime gameTime)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            
            if (myplayer1.Position.X <= position.X)
            { velocity.X = 2f;  }
            if (myplayer1.Position.X >= position.X)
            { velocity.X = -2f; }
            
            if (timer > 600)
            { velocity.X = 0; }


            if (timer > 1400)
            { timer = 0; enemieState = EnemieState.isPatrolling; BackFromEvading = false; }

            

        }

        public void isAware(GameTime gameTime)
        {

            if (jumpAware == false)
            { velocity.X = 0; position.Y -= 0f; velocity.Y = -0f; jumpAware = true; SoundFx.PlayFraisiMini(); }
            Exclamation = true;

            
        }

        public void Animation()
        {
            frameRectangleRight = new Rectangle(currentFrameRight * spriteWidth, 0, spriteWidth, spriteHeight);
            frameRectangleLeft = new Rectangle(currentFrameLeft * spriteWidth, 0, spriteWidth, spriteHeight);
            frameRectangleJumpRight = new Rectangle(currentJumpFrameRight * spriteWidth, 0, spriteWidth, spriteHeight);
            frameRectangleJumpLeft = new Rectangle(currentJumpFrameLeft * spriteWidth, 0, spriteWidth, spriteHeight);

            if (animationState == AnimationState.walkingRight)
            {
                if (currentFrameRight > 7) currentFrameRight = 4;  //Éviter qu'il dépasse la limite permise
                                
                    if (timer > interval)
                    {
                        currentFrameRight++;
                        timer = 0;
                    }
                    if (currentFrameRight == 7)
                    {
                        currentFrameRight = 4;
                    }
            }
            if (animationState == AnimationState.runningRight)
            {
                if (currentFrameRight > 7) currentFrameRight = 4;  //Éviter qu'il dépasse la limite permise

                if (timer > interval2)
                {
                    currentFrameRight++;
                    timer = 0;
                }
                if (currentFrameRight == 7)
                {
                    currentFrameRight = 4;
                }
            }

            if (animationState == AnimationState.walkingLeft)
            {
                if (currentFrameLeft > 3) currentFrameLeft = 0;  //Éviter qu'il dépasse la limite permise


                if (timer > interval)
                {
                    currentFrameLeft++;
                    timer = 0;
                }
                if (currentFrameLeft == 3)
                {
                    currentFrameLeft = 0;
                }
            }
            if (animationState == AnimationState.runningLeft)
            {
                if (currentFrameLeft > 3) currentFrameLeft = 0;  //Éviter qu'il dépasse la limite permise


                if (timer > interval2)
                {
                    currentFrameLeft++;
                    timer = 0;
                }
                if (currentFrameLeft == 3)
                {
                    currentFrameLeft = 0;
                }
            }
        }
        



    }
}
